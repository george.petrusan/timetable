# Dezvoltarea unei aplicații web pentru gestionarea orarului

### Candidat: George-Cristian PETRUȘAN
### Coordonator: Ș.l. dr. ing. Daniel IERCAN

### [Link aplicatie](https://github.com/UPT-ERP/timetable)

## Pași de rulare:
### Interfața utilizator
În terminal, navigăm în folderul timetable-ui și rulăm comanda 'npm start'

```bash
npm start
```
###### Aplicația UI rulează pe localhost:3000

### Backend
Cu ajutorul Visual Studio, deschidem timetable-api2 > timetable-api2.sln și rulăm aplicația. 

###### Aplicația API rulează pe localhost:7174
