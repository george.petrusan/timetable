Proiect: Dezvoltarea unei aplicații web pentru gestionarea orarului

Candidat: George-Cristian PETRUȘAN
Coordonator: Ș.l. dr. ing. Daniel IERCAN

Link aplicatie: https://github.com/UPT-ERP/timetable

Pași de rulare:
- interfața utilizator: în terminal, navigăm în folderul timetable-ui și rulăm comanda 'npm start'. Aplicația UI rulează pe localhost:3000
- backend: cu ajutorul Visual Studio, deschidem timetable-api2 > timetable-api2.sln și rulăm aplicația. Aplicația API rulează pe localhost:7174
