﻿namespace timetable_api2.Models
{
    public class Student : IPerson
    {
        public string? MatriculationCode { get; set; }
        public string? Department { get; set; }
        public int? StudyYear { get; set; }
        public double? GeneralAverage { get; set; }
        public List<string>? Subjects { get; set; }
        public List<string>? AppointmentIds { get; set; }
        public Dictionary<string, List<string>>? OptionalAppointments { get; set; }
    }
}
