﻿using System.Security.Cryptography.X509Certificates;

namespace timetable_api2.Models
{
    public class Teacher : IPerson
    {
        public string? Department { get; set; }
        public List<string>? AppointmentIds { get; set; }

    }
}
