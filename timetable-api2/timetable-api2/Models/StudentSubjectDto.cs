﻿namespace timetable_api2.Models
{    public class StudentSubjectDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

}
