﻿namespace timetable_api2.Models
{
    public class Appointment
    {
        public string? Title { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string? Id { get; set; }
        public string? RRule { get; set; }
        public string? ExDate { get; set; }
        public bool? AllDay { get; set; }
        public string? Notes { get; set; }
        public int? Capacity { get; set; }
        public string? Teacher { get; set; }
    }

}
