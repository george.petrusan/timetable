﻿namespace timetable_api2.Models
{
    public class Repartition
    {
        public string? MatriculationCode { get; set; }
        public string? Name { get; set; }
        public double? GeneralAverage { get; set; }
        public string? OptionName { get; set; }
    }
}
