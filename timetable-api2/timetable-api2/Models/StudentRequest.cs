﻿namespace timetable_api2.Models
{
    public class StudentRequest : IPerson
    {
        public string? MatriculationCode { get; set; }
    }
}
