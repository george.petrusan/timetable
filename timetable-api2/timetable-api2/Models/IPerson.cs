﻿namespace timetable_api2.Models
{
    public class IPerson
    {
        public string? Id { get; set; } 
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Discriminator { get; }
    }
}
