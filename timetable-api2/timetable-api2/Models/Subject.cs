﻿namespace timetable_api2.Models
{
    public class Subject
    {
        public string? Id { get; set; }
        public string? Name { get; set; }
        public string? StudyYear { get; set; }
        public List<string>? Appointments { get; set; }
        public List<string>? OptionalAppointments { get; set; }
    }
}
