﻿using Microsoft.EntityFrameworkCore;
using timetable_api2.Models;

namespace timetable_api2.Data
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Student>? Students { get; set; }
        public DbSet<Teacher>? Teachers { get; set; }
        public DbSet<Administrator>? Administrators { get; set; }
        public DbSet<Appointment>? Appointments { get; set; }
        public DbSet<Subject>? Subjects { get; set; }
        public DbSet<StudentRequest>? StudentRequests { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseCosmos(
                "https://azure-cosmosdb-licenta-cristi.documents.azure.com:443/",
                "aF0UwNevRibz6q1mlKpg5233fnIRlYo9VcFjRR6wqydFk0dq8GzBcAgWNHlgOdW7bojX0lH6BRMRACDbjlfDgQ==",
                "cristi-database");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>()
                .ToContainer("cristi-students")
                .HasPartitionKey(e => e.Id);
            modelBuilder.Entity<Teacher>()
                .ToContainer("cristi-students")
                .HasPartitionKey(e => e.Id);
            modelBuilder.Entity<Administrator>()
                .ToContainer("cristi-students")
                .HasPartitionKey(e => e.Id);
            modelBuilder.Entity<StudentRequest>()
                .ToContainer("cristi-students")
                .HasPartitionKey(e => e.Id);

            modelBuilder.Entity<Appointment>()
                .ToContainer("cristi-appointments")
                .HasPartitionKey(e => e.Id);
            modelBuilder.Entity<Subject>()
                .ToContainer("cristi-appointments")
                .HasPartitionKey(e => e.Id);
        }
    }
}
