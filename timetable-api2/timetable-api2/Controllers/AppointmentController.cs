﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mail;
using System.Net;
using timetable_api2.Data;
using timetable_api2.Models;
using System.Text;
using Ical.Net;
using Ical.Net.CalendarComponents;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using System.Security.AccessControl;
using Microsoft.Extensions.Logging;
using NodaTime;
using System.Globalization;

namespace timetable_api2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {
        private DatabaseContext _databaseContext = new DatabaseContext();

        [HttpGet]
        public async Task<IEnumerable<Appointment>> Get()
        {
            List<Appointment> appointments = new List<Appointment>();
            if (_databaseContext != null)
            {
                appointments = await _databaseContext.Appointments.ToListAsync();
            }

            return appointments;
        }

        [HttpGet("titleList")]
        public async Task<IEnumerable<string>> GetTitleList()
        {
            List<string> appointmentTitles = new List<string>();
            if (_databaseContext != null)
            {
                appointmentTitles = await _databaseContext.Appointments
                    .Select(a => a.Title)
                    .OrderBy(title => title)
                    .ToListAsync();
                appointmentTitles.Distinct();
            }

            return appointmentTitles;
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] List<Appointment> appointments)
        {
            if (appointments == null || appointments.Count == 0)
            {
                return BadRequest("Invalid request body.");
            }

            foreach (var appointment in appointments)
            {
                if (appointment.Id == null)
                    appointment.Id = Guid.NewGuid().ToString(); // Generate a new GUID as the student ID
                await _databaseContext.Appointments.AddAsync(appointment);
            }

            await _databaseContext.SaveChangesAsync();

            return Ok();
        }
        [HttpPost("addAppointment")]
        public async Task<IActionResult> Post([FromBody] Appointment appointment)
        {
            if (appointment == null)
            {
                return BadRequest("Invalid request body.");
            }

            if (appointment.Id == null)
                appointment.Id = Guid.NewGuid().ToString(); // Generate a new GUID as the student ID

            await _databaseContext.Appointments.AddAsync(appointment);
            await _databaseContext.SaveChangesAsync();

            return Ok();
        }

        [HttpPut("{name}")]
        public async Task<IActionResult> UpdateBasedOnName(string name, [FromBody] Appointment appointment)
        {
            if (appointment == null || name != appointment.Title)
            {
                return BadRequest("Invalid request body.");
            }

            var existingAppointment = await _databaseContext.Appointments.FirstOrDefaultAsync(a => a.Title == name);

            if (existingAppointment == null)
            {
                existingAppointment = new Appointment // create new appointment
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = appointment.Title,
                    StartDate = appointment.StartDate,
                    EndDate = appointment.EndDate,
                    RRule = appointment.RRule,
                    ExDate = appointment.ExDate,
                    AllDay = appointment.AllDay,
                    Capacity = appointment.Capacity,
                    Teacher = appointment.Teacher
                };

                _databaseContext.Appointments.Add(existingAppointment); // add to context

                await _databaseContext.SaveChangesAsync(); // save changes to database

                return StatusCode(200, "Resource created successfully");
            }
            else // update existing appointment
            {
                existingAppointment.Title = appointment.Title;
                existingAppointment.StartDate = appointment.StartDate;
                existingAppointment.EndDate = appointment.EndDate;
                existingAppointment.RRule = appointment.RRule;
                existingAppointment.ExDate = appointment.ExDate;
                existingAppointment.AllDay = appointment.AllDay;
                existingAppointment.Notes = appointment.Notes;
                existingAppointment.Capacity = appointment.Capacity;
                existingAppointment.Teacher = appointment.Teacher;

                await _databaseContext.SaveChangesAsync(); // save changes to database

                return StatusCode(200, "Resource updated successfully");
            }
        }

        [HttpPut("updatebasedonid/{id}")]
        public async Task<IActionResult> UpdateBasedOnId(string id, [FromBody] Appointment appointment)
        {
            if (appointment == null || id != appointment.Id)
            {
                return BadRequest("Invalid request body.");
            }

            var existingAppointment = await _databaseContext.Appointments.FindAsync(id);

            if (existingAppointment == null)
            {
                return NotFound("Appointment not found.");
            }

            if (appointment.Title != null)
            { 
                string oldTitle = existingAppointment.Title;
                existingAppointment.Title = appointment.Title;
                var students = await _databaseContext.Students.ToListAsync();

                foreach (var student in students)
                {
                    if (student.AppointmentIds != null && student.AppointmentIds.Contains(oldTitle))
                    {
                        student.AppointmentIds.Remove(oldTitle);
                        student.AppointmentIds.Add(existingAppointment.Title);

                        // Send email to the student
                        string studentEmail = student.Email;
                        string emailSubject = "Appointment Update";
                        string emailBody = $"Dear {student.Name},\n\nYour appointment with the title '{oldTitle}' has been updated. The new title is '{existingAppointment.Title}'.\n\nPlease check your account for further details.";

                        await SendEmail(studentEmail, emailSubject, emailBody);

                    }
                }
            }
            if (appointment.StartDate != null) 
                existingAppointment.StartDate = appointment.StartDate;
            if (appointment.EndDate != null)
                existingAppointment.EndDate = appointment.EndDate;
            if (appointment.RRule != null)
                existingAppointment.RRule = appointment.RRule;
            if (appointment.ExDate != null)
                existingAppointment.ExDate = appointment.ExDate;
            if (appointment.AllDay != null)
                existingAppointment.AllDay = appointment.AllDay;
            if (appointment.Notes != null)
                existingAppointment.Notes = appointment.Notes;
            if (appointment.Capacity != null)
                existingAppointment.Capacity = appointment.Capacity;
            if (appointment.Teacher != null)
                existingAppointment.Teacher = appointment.Teacher;

            await _databaseContext.SaveChangesAsync(); // save changes to database

            return StatusCode(200, new { message = "Resource updated successfully" });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            Appointment? appointment = await _databaseContext.Appointments.FindAsync(id);

            if (appointment == null)
            {
                return NotFound();
            }

            _databaseContext.Appointments.Remove(appointment);
            await _databaseContext.SaveChangesAsync();

            return StatusCode(200, "Resource deleted successfully");
        }

        [HttpGet("getAppointmentsForStudent/{email}")]
        public async Task<IEnumerable<Appointment>> GetAppointmentsByStudent(string email)
        {
            var student = await _databaseContext.Students.FirstOrDefaultAsync(s => s.Email == email);
            if (student == null)
            {
                return (IEnumerable<Appointment>)NotFound();
            }

            var appointments = await _databaseContext.Appointments
                .Where(a => student.AppointmentIds != null && student.AppointmentIds.Contains(a.Title))
                .ToListAsync();
            return appointments;
        }

        [HttpGet("getAppointmentsForTeacher/{email}")]
        public async Task<IEnumerable<Appointment>> GetAppointmentsByTeacher(string email)
        {
            var teacher = await _databaseContext.Teachers.FirstOrDefaultAsync(s => s.Email == email);
            if (teacher == null)
            {
                return (IEnumerable<Appointment>)NotFound();
            }

            var appointments = await _databaseContext.Appointments
                .Where(a => teacher.AppointmentIds != null && teacher.AppointmentIds.Contains(a.Title))
                .ToListAsync();
            return appointments;
        }


        [HttpPost("import/no-overwrite")]
        public async Task<IActionResult> ImportWithoutOverwrite(IEnumerable<Appointment> appointments)
        {
            try
            {
                foreach (var appointment in appointments)
                {
                    if (string.IsNullOrEmpty(appointment.Id))
                    {
                        appointment.Id = Guid.NewGuid().ToString(); // Generate a unique ID
                    }
                    var existingAppointment = await _databaseContext.Appointments.FindAsync(appointment.Id);
                    if (existingAppointment == null)
                    {
                        _databaseContext.Appointments.Add(appointment);
                    }
                }

                await _databaseContext.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                // Handle any exceptions and return an appropriate response
                return StatusCode(500, $"An error occurred during import: {ex.Message}");
            }
        }

        [HttpPost("import/overwrite")]
        public async Task<IActionResult> ImportWithOverwrite(IEnumerable<Appointment> appointments)
        {
            try
            {
                foreach (var appointment in appointments)
                {
                    if (string.IsNullOrEmpty(appointment.Id))
                    {
                        appointment.Id = Guid.NewGuid().ToString(); // Generate a unique ID
                    }
                    var existingAppointment = await _databaseContext.Appointments.FindAsync(appointment.Id);
                    if (existingAppointment != null)
                    {
                        _databaseContext.Entry(existingAppointment).CurrentValues.SetValues(appointment);
                    }
                    else
                    {
                        _databaseContext.Appointments.Add(appointment);
                    }
                }

                await _databaseContext.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                // Handle any exceptions and return an appropriate response
                return StatusCode(500, $"An error occurred during import: {ex.Message}");
            }
        }
        [HttpPost("import/with-deletion")]
        public async Task<IActionResult> ImportWithDeletion(IEnumerable<Appointment> appointments)
        {
            try
            {
                var existingAppointments = await _databaseContext.Appointments.ToListAsync();

                // Delete existing appointments not present in the imported JSON
                var appointmentsToDelete = existingAppointments.Where(a => !appointments.Any(ap => ap.Id == a.Id)).ToList();
                _databaseContext.Appointments.RemoveRange(appointmentsToDelete);

                // Add or update imported appointments
                foreach (var appointment in appointments)
                {
                    var existingAppointment = existingAppointments.FirstOrDefault(a => a.Id == appointment.Id);
                    if (existingAppointment != null)
                    {
                        _databaseContext.Entry(existingAppointment).CurrentValues.SetValues(appointment);
                    }
                    else
                    {
                        _databaseContext.Appointments.Add(appointment);
                    }
                }

                await _databaseContext.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                // Handle any exceptions and return an appropriate response
                return StatusCode(500, $"An error occurred during import: {ex.Message}");
            }
        }

        [HttpGet("csv")]
        public async Task<IActionResult> ExportAppointmentsCSV()
        {
            var appointments = await _databaseContext.Appointments.ToListAsync();

            if (appointments.Count == 0)
            {
                return NoContent();
            }

            var csvBuilder = new StringBuilder();
            csvBuilder.AppendLine("Id,Title,StartDate,EndDate,RRule,ExDate,AllDay,Capacity,Notes");

            foreach (var appointment in appointments)
            {
                csvBuilder.AppendLine($"{EscapeCsvField(appointment.Id)},{EscapeCsvField(appointment.Title)},{appointment.StartDate.ToString()},{appointment.EndDate.ToString()},{EscapeCsvField(appointment.RRule)},{EscapeCsvField(appointment.ExDate)},{appointment.AllDay},{EscapeCsvField(appointment.Capacity.ToString())},{EscapeCsvField(appointment.Notes)}");
            }

            var csvBytes = Encoding.UTF8.GetBytes(csvBuilder.ToString());

            var memoryStream = new MemoryStream(csvBytes);
            memoryStream.Seek(0, SeekOrigin.Begin);

            var response = File(memoryStream, "text/csv", "appointments.csv");

            return response;
        }

        private string EscapeCsvField(string fieldValue)
        {
            if (fieldValue == null)
            {
                return string.Empty;
            }

            if (fieldValue.Contains(",") || fieldValue.Contains("\"") || fieldValue.Contains("\r") || fieldValue.Contains("\n"))
            {
                fieldValue = fieldValue.Replace("\"", "\"\"");
                fieldValue = $"\"{fieldValue}\"";
            }

            return fieldValue;
        }

        [HttpGet("getUpcomingAppointmentsForStudent/{email}")]
        public async Task<IActionResult> GetUpcomingAppointmentsByStudent(string email)
        {
            var student = await _databaseContext.Students.FirstOrDefaultAsync(s => s.Email == email);
            if (student == null)
            {
                return NotFound();
            }

            List<Appointment> appointments = await _databaseContext.Appointments
                .Where(a => student.AppointmentIds != null && student.AppointmentIds.Contains(a.Title))
                .ToListAsync();

            List<Appointment> upcomingAppointments = new List<Appointment>();

            var now = DateTime.Now;
            var nextWeek = now.AddDays(7);

            foreach (var appointment in appointments)
            {
                var calendar = new Ical.Net.Calendar();
                var eventItem = new CalendarEvent
                {
                    Start = new CalDateTime(appointment.StartDate.Value),
                    End = new CalDateTime(appointment.EndDate.Value),
                    RecurrenceRules = !string.IsNullOrEmpty(appointment.RRule) ? new List<RecurrencePattern> { new RecurrencePattern(appointment.RRule) } : null
                };

                // Add ExDate
                if (!string.IsNullOrEmpty(appointment.ExDate))
                {
                    eventItem.ExceptionDates.Add(new PeriodList { new CalDateTime(DateTime.Parse(appointment.ExDate)) });
                }

                calendar.Events.Add(eventItem);

                var occurrences = calendar.GetOccurrences(now, nextWeek);
                if (occurrences.Any())
                {
                    upcomingAppointments.Add(appointment);
                }
            }

            return Ok(upcomingAppointments);
        }

        [HttpGet("getICalendarForStudent/{email}")]
        public async Task<IActionResult> GetICalendarForStudent(string email)
        {
            var student = await _databaseContext.Students.FirstOrDefaultAsync(s => s.Email == email);
            if (student == null)
            {
                return NotFound();
            }

            List<Appointment> appointments = await _databaseContext.Appointments
                .Where(a => student.AppointmentIds != null && student.AppointmentIds.Contains(a.Title))
                .ToListAsync();

            var calendar = new Ical.Net.Calendar();

            foreach (var appointment in appointments)
            {
                var evt = new CalendarEvent
                {
                    Summary = appointment.Title,
                    Description = appointment.Notes, // Add this line to include the notes
                    Start = new CalDateTime(appointment.StartDate.Value),
                    End = new CalDateTime(appointment.EndDate.Value),
                    Uid = appointment.Id
                };

                // Convert rRule from string to RecurrencePattern
                if (!string.IsNullOrEmpty(appointment.RRule))
                {
                    var recurrence = new RecurrencePattern(appointment.RRule);
                    evt.RecurrenceRules.Add(recurrence);
                }

                // Convert exDate from string to iCalDateTime and add it to the ExceptionDates list
                if (!string.IsNullOrEmpty(appointment.ExDate))
                {
                    var exceptionDate = new CalDateTime(DateTime.Parse(appointment.ExDate));
                    evt.ExceptionDates.Add(new PeriodList { exceptionDate });
                }
                calendar.Events.Add(evt);
            }

            // Serialize the calendar to string
            var serializer = new CalendarSerializer();
            var serializedCalendar = serializer.SerializeToString(calendar);

            // Return the serialized calendar
            return Ok(serializedCalendar);
        }

        [HttpGet("getICalendarForTeacher/{email}")]
        public async Task<IActionResult> GetICalendarForTeacher(string email)
        {
            var teacher = await _databaseContext.Teachers.FirstOrDefaultAsync(s => s.Email == email);
            if (teacher == null)
            {
                return NotFound();
            }

            List<Appointment> appointments = await _databaseContext.Appointments
                .Where(a => teacher.AppointmentIds != null && teacher.AppointmentIds.Contains(a.Title))
                .ToListAsync();

            var calendar = new Ical.Net.Calendar();

            foreach (var appointment in appointments)
            {
                var evt = new CalendarEvent
                {
                    Summary = appointment.Title,
                    Description = appointment.Notes, // Add this line to include the notes
                    Start = new CalDateTime(appointment.StartDate.Value),
                    End = new CalDateTime(appointment.EndDate.Value),
                    Uid = appointment.Id
                };

                // Convert rRule from string to RecurrencePattern
                if (!string.IsNullOrEmpty(appointment.RRule))
                {
                    var recurrence = new RecurrencePattern(appointment.RRule);
                    evt.RecurrenceRules.Add(recurrence);
                }

                // Convert exDate from string to iCalDateTime and add it to the ExceptionDates list
                if (!string.IsNullOrEmpty(appointment.ExDate))
                {
                    var exceptionDate = new CalDateTime(DateTime.Parse(appointment.ExDate));
                    evt.ExceptionDates.Add(new PeriodList { exceptionDate });
                }
                calendar.Events.Add(evt);
            }

            // Serialize the calendar to string
            var serializer = new CalendarSerializer();
            var serializedCalendar = serializer.SerializeToString(calendar);

            // Return the serialized calendar
            return Ok(serializedCalendar);
        }




        private async Task SendEmail(string recipientEmail, string subject, string body)
        {
            try
            {
                // Set the sender's email address and credentials
                string senderEmail = "george.cristian.petrusan@gmail.com";
                string senderPassword = "numaenerva123";
                string smtpServer = "smtp.gmail.com";
                int smtpPort = 587;

                // Create a new MailMessage
                MailMessage mail = new MailMessage(senderEmail, recipientEmail);
                mail.Subject = subject;
                mail.Body = body;

                // Set the SMTP client and its credentials
                SmtpClient smtpClient = new SmtpClient(smtpServer, smtpPort);
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new NetworkCredential(senderEmail, senderPassword);

                // Send the email
                await smtpClient.SendMailAsync(mail);
            }
            catch (Exception ex)
            {
                // Handle any errors or log them appropriately
                Console.WriteLine("An error occurred while sending the email: " + ex.Message);
            }
        }


    }
}
