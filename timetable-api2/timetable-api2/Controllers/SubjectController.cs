﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using timetable_api2.Data;
using timetable_api2.Models;

namespace timetable_api2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SubjectController : ControllerBase
    {
        private DatabaseContext _databaseContext = new DatabaseContext();

        // GET: /subject
        [HttpGet]
        public async Task<IActionResult> GetSubjects()
        {
            var subjects = await _databaseContext.Subjects.ToListAsync();
            return Ok(subjects);
        }

        // GET: /subject?studyYear={studyYear}
        [HttpGet("studyYear")]
        public async Task<IActionResult> GetSubjectsByStudyYear(string studyYear)
        {
            var subjects = await _databaseContext.Subjects
                .Where(s => s.StudyYear == studyYear)
                .ToListAsync();
            return Ok(subjects);
        }

        // GET: /student/list
        [HttpGet("list")]
        public async Task<IActionResult> GetStudentSubjects()
        {
            var subjects = await _databaseContext.Subjects.ToListAsync();

            var studentSubjectDtos = subjects.Select(subject => new StudentSubjectDto
            {
                Key = subject.Name.Trim().Replace(" ", "").ToLower(),
                Value = subject.Name
            }).ToList();

            return Ok(studentSubjectDtos);
        }

        // POST: /subject
        [HttpPost]
        public async Task<IActionResult> AddSubjects(IEnumerable<Subject> subjects)
        {
            try
            {
                foreach (var subject in subjects)
                {
                    subject.Id = Guid.NewGuid().ToString(); // Generate a new GUID for each subject
                    _databaseContext.Subjects.Add(subject);
                }

                await _databaseContext.SaveChangesAsync();

                return StatusCode(200, new { message = "Resource/s created successfully" });
            }
            catch (Exception ex)
            {
                // Handle any exceptions and return an appropriate response
                return StatusCode(500, $"An error occurred during subject addition: {ex.Message}");
            }
        }

        // PUT: /subject/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateSubject(string id, [FromBody]  Subject updatedSubject)
        {
            var subject = await _databaseContext.Subjects.FindAsync(id);
            if (subject == null)
            {
                return NotFound();
            }

            subject.Name = updatedSubject.Name;
            subject.StudyYear = updatedSubject.StudyYear;
            subject.Appointments = updatedSubject.Appointments;
            subject.OptionalAppointments = updatedSubject.OptionalAppointments;

            await _databaseContext.SaveChangesAsync();
            return StatusCode(200, new { message = "Resource updated successfully" });
        }

        // DELETE: /subject/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubject(string id)
        {
            var subject = await _databaseContext.Subjects.FindAsync(id);
            if (subject == null)
            {
                return NotFound();
            }

            _databaseContext.Subjects.Remove(subject);
            await _databaseContext.SaveChangesAsync();
            return StatusCode(200, new { message = "Resource deleted successfully" });
        }

        // GET: /subject/repartition/{subjectName}
        [HttpGet("repartition/{subjectName}")]
        public async Task<IActionResult> GetRepartition(string subjectName)
        {
            var students = await _databaseContext.Students.ToListAsync();
            var appointments = await _databaseContext.Appointments
                .Where(a => a.Title.StartsWith(subjectName))
                .ToListAsync();
            var repartition = new List<Repartition>();

            // Only consider students that have this subject
            var relevantStudents = students
                .Where(s => s.Subjects != null && s.Subjects.Contains(subjectName))
                .ToList();

            // Sort descending by GeneralAverage
            relevantStudents.Sort((x, y) =>
            {
                if (x.GeneralAverage == null && y.GeneralAverage == null)
                {
                    return 0;
                }
                if (x.GeneralAverage == null)
                {
                    return 1;
                }
                if (y.GeneralAverage == null)
                {
                    return -1;
                }
                return y.GeneralAverage.Value.CompareTo(x.GeneralAverage.Value);
            });

            // Initialize capacities
            var capacities = new Dictionary<string, int>();
            var maxCapacities = appointments.ToDictionary(a => a.Title, a => a.Capacity);

            foreach (var student in relevantStudents)
            {
                if (!student.OptionalAppointments.ContainsKey(subjectName))
                {
                    continue;
                }

                foreach (var option in student.OptionalAppointments[subjectName])
                {
                    if (!capacities.ContainsKey(option))
                    {
                        capacities[option] = 0;
                    }
                }
            }

            foreach (var student in relevantStudents)
            {
                if (!student.OptionalAppointments.ContainsKey(subjectName))
                {
                    continue;
                }

                var allocated = false;

                // Go through the options in the order of preference until one with remaining capacity is found
                foreach (var option in student.OptionalAppointments[subjectName])
                {
                    if (capacities[option] < maxCapacities[option])
                    {
                        repartition.Add(new Repartition
                        {
                            MatriculationCode = student.MatriculationCode,
                            Name = student.Name,
                            GeneralAverage = student.GeneralAverage,
                            OptionName = option
                        });

                        capacities[option]++;
                        allocated = true;
                        break;
                    }
                }

                // Handle case where no option with remaining capacity was found for this student
                if (!allocated)
                {
                    // Depending on your requirements, you could either leave this student unallocated, or allocate to the least-filled option, etc.
                    repartition.Add(new Repartition
                    {
                        MatriculationCode = student.MatriculationCode,
                        Name = student.Name,
                        GeneralAverage = student.GeneralAverage,
                        OptionName = "unallocated"
                    });
                }
            }

            return Ok(repartition);
        }

        // POST: /subject/makeRepartition/{subjectName}
        [HttpPost("makeRepartition/{subjectName}")]
        public async Task<IActionResult> MakeRepartition(string subjectName, [FromBody] List<Repartition> repartition)
        {
            try
            {
                var subject = await _databaseContext.Subjects.FirstOrDefaultAsync(s => s.Name == subjectName);
                if (subject == null)
                {
                    return NotFound();
                }

                var students = await _databaseContext.Students.ToListAsync();

                foreach (var student in students)
                {
                    var repartitionInfo = repartition.FirstOrDefault(r => r.MatriculationCode == student.MatriculationCode);
                    if (repartitionInfo != null && repartitionInfo.OptionName != "unallocated")
                    {
                        student.AppointmentIds ??= new List<string>();
                        student.AppointmentIds.Add(repartitionInfo.OptionName);
                    }
                }

                await _databaseContext.SaveChangesAsync();

                return StatusCode(200, new { message = "Repartition updated successfully" });
            }
            catch (Exception ex)
            {
                // Handle any exceptions and return an appropriate response
                return StatusCode(500, $"An error occurred during repartition update: {ex.Message}");
            }
        }

        // GET: /subject/names
        [HttpGet("names")]
        public async Task<IActionResult> GetSubjectNames()
        {
            var subjects = await _databaseContext.Subjects.ToListAsync();
            var subjectNames = subjects.Select(subject => subject.Name).ToList();
            return Ok(subjectNames);
        }

        // GET: /subject/withoutAppointment
        [HttpGet("withoutAppointment")]
        public async Task<IActionResult> GetSubjectsWithoutAppointment()
        {
            try
            {
                var subjects = await _databaseContext.Subjects.ToListAsync();
                var subjectsWithoutAppointment = new List<string>();
                var appointments = await _databaseContext.Appointments.ToListAsync();

                foreach (Subject subject in subjects)
                {
                    if (subject.Appointments != null)
                    {
                        foreach (var appointment in subject.Appointments)
                        {
                            if (!appointments.Any(a => a.Title.StartsWith(appointment)))
                            {
                                subjectsWithoutAppointment.Add(appointment);
                            }
                        }
                    }

                    if (subject.OptionalAppointments != null)
                    {
                        foreach (var optionalAppointment in subject.OptionalAppointments)
                        {
                            if (!appointments.Any(a => a.Title.StartsWith(optionalAppointment)))
                            {
                                subjectsWithoutAppointment.Add(optionalAppointment);
                            }
                        }
                    }
                }

                return Ok(subjectsWithoutAppointment);
            }
            catch (Exception ex)
            {
                // Handle any exceptions and return an appropriate response
                return StatusCode(500, $"An error occurred while retrieving subjects without appointment: {ex.Message}");
            }
        }





    }
}
