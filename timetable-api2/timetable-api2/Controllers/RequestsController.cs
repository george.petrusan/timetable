﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using timetable_api2.Data;
using timetable_api2.Models;

namespace timetable_api2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StudentRequestController : ControllerBase
    {
        private DatabaseContext _databaseContext = new DatabaseContext();

        [HttpGet("students")]
        public async Task<IEnumerable<StudentRequest>> Get()
        {
            return await _databaseContext.StudentRequests.ToListAsync();
        }

        [HttpPost("students")]
        public async Task<IActionResult> Post([FromBody] List<StudentRequest> studentRequests)
        {
            if (studentRequests == null || studentRequests.Count == 0)
            {
                return BadRequest("Invalid request body.");
            }

            foreach (var studentRequest in studentRequests)
            {
                if (studentRequest.Id == null)
                    studentRequest.Id = Guid.NewGuid().ToString(); // Generate a new GUID as the student request ID
                await _databaseContext.StudentRequests.AddAsync(studentRequest);
            }

            await _databaseContext.SaveChangesAsync();

            return Ok();
        }

        [HttpPut("students/updatebasedonemail/{email}")]
        public async Task<IActionResult> UpdateBasedOnEmail(string email, [FromBody] StudentRequest studentRequest)
        {
            if (studentRequest == null || email != studentRequest.Email)
            {
                return BadRequest("Invalid request body.");
            }

            var existingStudentRequest = await _databaseContext.StudentRequests.FirstOrDefaultAsync(s => s.Email == email);

            if (existingStudentRequest == null)
            {
                return NotFound("Student request not found.");
            }

            if (studentRequest.Name != null)
                existingStudentRequest.Name = studentRequest.Name;
            if (studentRequest.MatriculationCode != null)
                existingStudentRequest.MatriculationCode = studentRequest.MatriculationCode;

            await _databaseContext.SaveChangesAsync();

            return StatusCode(200, new { message = "Resource updated successfully" });
        }

        [HttpGet("students/{email}")]
        public async Task<IActionResult> GetByEmail(string email)
        {
            var studentRequest = await _databaseContext.StudentRequests.FirstOrDefaultAsync(s => s.Email == email);

            if (studentRequest == null)
            {
                return NotFound("Student request not found.");
            }

            return Ok(studentRequest);
        }

        [HttpDelete("students/{email}")]
        public async Task<IActionResult> DeleteByEmail(string email)
        {
            var studentRequest = await _databaseContext.StudentRequests.FirstOrDefaultAsync(s => s.Email == email);
            if (studentRequest == null)
            {
                return NotFound();
            }

            _databaseContext.StudentRequests.Remove(studentRequest);
            await _databaseContext.SaveChangesAsync();

            return Ok(studentRequest);
        }
    }
}
