﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using timetable_api2.Data;
using timetable_api2.Models;

namespace timetable_api2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TeacherController : ControllerBase
    {
        private DatabaseContext _databaseContext = new DatabaseContext();

        [HttpGet]
        public async Task<IEnumerable<Teacher>> Get()
        {
            List<Teacher> teachers = new List<Teacher>();
            if (_databaseContext != null)
            {
                teachers = await _databaseContext.Teachers.ToListAsync();
            }

            return teachers;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] List<Teacher> teachers)
        {
            if (teachers == null || teachers.Count == 0)
            {
                return BadRequest("Invalid request body.");
            }

            foreach (var teacher in teachers)
            {
                if (teacher.Id == null)
                    teacher.Id = Guid.NewGuid().ToString(); // Generate a new GUID as the teacher ID
                await _databaseContext.Teachers.AddAsync(teacher);
            }

            await _databaseContext.SaveChangesAsync();

            return Ok();
        }

        // PUT: Teacher/updatebasedonid/{id}
        [HttpPut("updatebasedonid/{id}")]
        public async Task<IActionResult> UpdateBasedOnId(string id, [FromBody] Teacher teacher)
        {
            if (teacher == null || id != teacher.Id)
            {
                return BadRequest("Invalid request body.");
            }

            var existingTeacher = await _databaseContext.Teachers.FindAsync(id);

            if (existingTeacher == null)
            {
                return NotFound("Teacher not found.");
            }

            if (teacher.Name != null)
                existingTeacher.Name = teacher.Name;
            if (teacher.Email != null)
                existingTeacher.Email = teacher.Email;
            if (teacher.AppointmentIds != null)
                existingTeacher.AppointmentIds = teacher.AppointmentIds;
            if (teacher.Department != null)
                existingTeacher.Department = teacher.Department;

            //update the other properties as needed

            await _databaseContext.SaveChangesAsync(); // save changes to database

            return StatusCode(200, new { message = "Resource updated successfully" });
        }

        // DELETE: Teacher/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var teacher = await _databaseContext.Teachers.FindAsync(id);
            if (teacher == null)
            {
                return NotFound();
            }

            _databaseContext.Teachers.Remove(teacher);
            await _databaseContext.SaveChangesAsync();

            return Ok(teacher);
        }
    }
}
