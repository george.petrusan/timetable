﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text;
using timetable_api2.Data;
using timetable_api2.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace timetable_api2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        // GET: api/<DataController>
        private DatabaseContext _databaseContext = new DatabaseContext();
        [HttpGet]
        public async Task<IEnumerable<Student>> Get()
        {
            List<Student> students = new List<Student>();
            if (_databaseContext != null)
            {
                students = await _databaseContext.Students.ToListAsync();
            }

            return students;
        }

        // GET api/<DataController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return id.ToString();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] List<Student> students)
        {
            if (students == null || students.Count == 0)
            {
                return BadRequest("Invalid request body.");
            }

            foreach (var student in students)
            {
                student.Id = Guid.NewGuid().ToString(); // Generate a new GUID as the student ID
                await _databaseContext.Students.AddAsync(student);
            }

            await _databaseContext.SaveChangesAsync();

            return StatusCode(200, new { message = "Resource/s created successfully" });
        }

        [HttpPost("studentRequest")]
        public async Task<IActionResult> Post([FromBody] StudentRequest studentRequest)
        {
            if (studentRequest == null)
            {
                return BadRequest("Invalid request body.");
            }

            // Check if a student with the same email already exists
            //bool studentExists = await _databaseContext.Students.AnyAsync(s => s.Email == studentRequest.Email);
            //if (studentExists)
            //{
            //    return StatusCode(500, new { message = "A student with the same email already exists." });
            //}

            Student newStudent = new Student
            {
                Id = Guid.NewGuid().ToString(),
                Name = studentRequest.Name,
                MatriculationCode = studentRequest.MatriculationCode,
                Email = studentRequest.Email
            };

            await _databaseContext.Students.AddAsync(newStudent);
            await _databaseContext.SaveChangesAsync();

            return StatusCode(200, new { message = "Resource/s created successfully" });
        }

        [HttpPut("updatebasedonid/{id}")]
        public async Task<IActionResult> UpdateBasedOnId(string id, [FromBody] Student student)
        {
            System.Diagnostics.Debug.WriteLine("salut UpdateBasedOnId");
            if (student == null || id != student.Id)
            {
                return BadRequest("Invalid request body.");
            }

            var existingStudent = await _databaseContext.Students.FindAsync(id);

            if (existingStudent == null)
            {
                return NotFound("Appointment not found.");
            }

            if (student.Id != null)
                existingStudent.Id = student.Id;
            if (student.Name != null)
                existingStudent.Name = student.Name;
            if (student.Department != null)
                existingStudent.Department = student.Department;
            if (student.StudyYear != null)
                existingStudent.StudyYear = student.StudyYear;
            //if (student.Age != null)
            //    existingStudent.Age = student.Age;
            if (student.MatriculationCode != null)
                existingStudent.MatriculationCode = student.MatriculationCode;
            if (student.Email != null)
                existingStudent.Email = student.Email;
            if (student.GeneralAverage != null)
                existingStudent.GeneralAverage = student.GeneralAverage;
            if (student.AppointmentIds != null)
                existingStudent.AppointmentIds = student.AppointmentIds;
            if (student.OptionalAppointments != null)
                existingStudent.OptionalAppointments = student.OptionalAppointments;
            if (student.Subjects != null)
            {
                // Get the existing subjects of the student
                var existingSubjects = existingStudent.Subjects ?? new List<string>();

                // Get the updated subjects from the request
                var updatedSubjects = student.Subjects;

                // Find the subjects that were removed
                var removedSubjects = existingSubjects.Except(updatedSubjects);
                foreach (var rsubject in removedSubjects)
                    System.Diagnostics.Debug.WriteLine(rsubject.ToString());

                // Find the subjects that were added
                var addedSubjects = updatedSubjects.Except(existingSubjects);

                // Update the AppointmentIds and OptionalAppointments based on the changes in subjects
                foreach (var removedSubject in removedSubjects)
                {
                    // Remove the corresponding appointments from AppointmentIds and OptionalAppointments
                    existingStudent.AppointmentIds.RemoveAll(appointmentId =>
                        appointmentId.Contains(removedSubject));

                    existingStudent.OptionalAppointments.Remove(removedSubject);
                }

                foreach (var addedSubject in addedSubjects)
                {
                    // Add the corresponding appointments to AppointmentIds and OptionalAppointments
                    var subject = await _databaseContext.Subjects.FirstOrDefaultAsync(s => s.Name == addedSubject);
                    if (subject != null)
                    {
                        existingStudent.AppointmentIds.AddRange(subject.Appointments);
                        foreach (var appointmentId in subject.OptionalAppointments)
                        {
                            if (existingStudent.OptionalAppointments.ContainsKey(addedSubject))
                            {
                                existingStudent.OptionalAppointments[addedSubject].Add(appointmentId);
                            }
                            else
                            {
                                existingStudent.OptionalAppointments[addedSubject] = new List<string> { appointmentId };
                            }
                        }
                    }
                }

                // Update the subjects of the student
                existingStudent.Subjects = updatedSubjects;
            }

            await _databaseContext.SaveChangesAsync(); // save changes to database

            return StatusCode(200, new { message = "Resource updated successfully" });
        }
        // DELETE api/<DataController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteByName(string id)
        {
            var students = await _databaseContext.Students.Where(s => s.Id == id).ToListAsync();

            if (students == null || students.Count == 0)
            {
                return NotFound();
            }

            _databaseContext.Students.RemoveRange(students);
            await _databaseContext.SaveChangesAsync();

            return StatusCode(200, new { message = "Resource deleted successfully" });
        }

        // GET: /students/email/{email}
        [HttpGet("email/{email}")]
        public async Task<IActionResult> GetByEmail(string email)
        {
            var student = await _databaseContext.Students.FirstOrDefaultAsync(s => s.Email == email);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        // GET: /students/email/{email}/optionalAppointments
        [HttpGet("email/{email}/optionalAppointments")]
        public async Task<IActionResult> GetOptionalAppointmentsByMatriculationCode(string email)
        {
            var student = await _databaseContext.Students.FirstOrDefaultAsync(s => s.Email == email);

            if (student == null)
            {
                return NotFound();
            }

            return Ok(student.OptionalAppointments);
        }

        // POST: /students/email/{email}/optionalAppointments
        [HttpPost("email/{email}/optionalAppointments")]
        public async Task<IActionResult> UpdateOptionalAppointmentsByMatriculationCode(string email, [FromBody] Dictionary<string, List<string>> optionalAppointments)
        {
            var student = await _databaseContext.Students.FirstOrDefaultAsync(s => s.Email == email);

            if (student == null)
            {
                return NotFound();
            }

            // Update the optionalAppointments of the student
            student.OptionalAppointments = optionalAppointments;

            // Save changes to the database
            await _databaseContext.SaveChangesAsync();

            return Ok();
        }

        // GET: /students/csv
        [HttpGet("csv")]
        public async Task<IActionResult> ExportStudentsCSV()
        {
            var students = await _databaseContext.Students.ToListAsync();

            if (students.Count == 0)
            {
                return NoContent();
            }

            var csvBuilder = new StringBuilder();
            csvBuilder.AppendLine("Id,MatriculationCode,Name,Department,StudyYear,Email,GeneralAverage,Subjects,AppointmentIds,OptionalAppointments");

            foreach (var student in students)
            {
                string subjects = student.Subjects != null ? string.Join(",", student.Subjects) : string.Empty;
                string appointmentIds = student.AppointmentIds != null ? string.Join(",", student.AppointmentIds) : string.Empty;
                string optionalAppointments = FormatOptionalAppointments(student.OptionalAppointments);

                csvBuilder.AppendLine($"{EscapeCsvField(student.Id)},{EscapeCsvField(student.MatriculationCode)},{EscapeCsvField(student.Name)},{EscapeCsvField(student.Department)},{student.StudyYear},{EscapeCsvField(student.Email)},{student.GeneralAverage},{EscapeCsvField(subjects)},{EscapeCsvField(appointmentIds)},{EscapeCsvField(optionalAppointments)}");
            }

            var csvBytes = Encoding.UTF8.GetBytes(csvBuilder.ToString());

            var memoryStream = new MemoryStream(csvBytes);
            memoryStream.Seek(0, SeekOrigin.Begin);

            var response = File(memoryStream, "text/csv", "students.csv");

            return response;
        }

        private string EscapeCsvField(string fieldValue)
        {
            if (fieldValue == null)
            {
                return string.Empty;
            }

            if (fieldValue.Contains(",") || fieldValue.Contains("\"") || fieldValue.Contains("\r") || fieldValue.Contains("\n"))
            {
                fieldValue = fieldValue.Replace("\"", "\"\"");
                fieldValue = $"\"{fieldValue}\"";
            }

            return fieldValue;
        }


        private string FormatOptionalAppointments(Dictionary<string, List<string>> optionalAppointments)
        {
            if (optionalAppointments == null || optionalAppointments.Count == 0)
            {
                return string.Empty;
            }

            var formattedOptionalAppointments = new StringBuilder();

            foreach (var entry in optionalAppointments)
            {
                string key = EscapeCsvField(entry.Key);
                string values = entry.Value != null ? string.Join(",", entry.Value) : string.Empty;

                formattedOptionalAppointments.AppendLine($"{key}:{values}");
            }

            return formattedOptionalAppointments.ToString();
        }

    }
}
