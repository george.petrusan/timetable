﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using timetable_api2.Data;
using timetable_api2.Models;

namespace timetable_api2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AdministratorController : ControllerBase
    {
        private DatabaseContext _databaseContext = new DatabaseContext();

        [HttpGet]
        public async Task<IEnumerable<Administrator>> Get()
        {
            List<Administrator> administrators = new List<Administrator>();
            if (_databaseContext != null)
            {
                administrators = await _databaseContext.Administrators.ToListAsync();
            }

            return administrators;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] List<Administrator> administrators)
        {
            if (administrators == null || administrators.Count == 0)
            {
                return BadRequest("Invalid request body.");
            }

            foreach (var Administrator in administrators)
            {
                if (Administrator.Id == null)
                    Administrator.Id = Guid.NewGuid().ToString(); // Generate a new GUID as the student ID
                await _databaseContext.Administrators.AddAsync(Administrator);
            }

            await _databaseContext.SaveChangesAsync();

            return Ok();
        }

        // PUT: Administrator
        [HttpPut("updatebasedonid/{id}")]
        public async Task<IActionResult> UpdateBasedOnId(string id, [FromBody] Administrator administrator)
        {
            if (administrator == null || id != administrator.Id)
            {
                return BadRequest("Invalid request body.");
            }

            var existingAdministrator = await _databaseContext.Administrators.FindAsync(id);

            if (existingAdministrator == null)
            {
                return NotFound("Administrator not found.");
            }

            if (administrator.Name != null)
                existingAdministrator.Name = administrator.Name;
            if (administrator.Email != null)
                existingAdministrator.Email = administrator.Email;

            await _databaseContext.SaveChangesAsync(); // save changes to database

            return StatusCode(200, new { message = "Resource updated successfully" });
        }
        // DELETE: Administrator/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var administrator = await _databaseContext.Administrators.FindAsync(id);
            if (administrator == null)
            {
                return NotFound();
            }

            _databaseContext.Administrators.Remove(administrator);
            await _databaseContext.SaveChangesAsync();

            return Ok(administrator);
        }

        // GET: Administrator/login/{email}
        [HttpGet("login/{email}")]
        public async Task<IActionResult> Login(string email)
        {
            var administrator = await _databaseContext.Administrators.FirstOrDefaultAsync(a => a.Email == email);
            if (administrator != null)
            {
                return Ok(new { role = "Administrator" });
            }

            var student = await _databaseContext.Students.FirstOrDefaultAsync(s => s.Email == email);
            if (student != null)
            {
                return Ok(new { role = "Student" });
            }

            var teacher = await _databaseContext.Teachers.FirstOrDefaultAsync(t => t.Email == email);
            if (teacher != null)
            {
                return Ok(new { role = "Teacher" });
            }

            return NotFound(new { message = "Email not found in any roles." });
        }



    }
}
