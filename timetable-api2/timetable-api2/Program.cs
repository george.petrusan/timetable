using Microsoft.EntityFrameworkCore;
using timetable_api2.Data;
using timetable_api2.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(options =>
{
    options.AddPolicy("CORSPolicy",
        builder =>
        {
            builder
            .WithOrigins("http://localhost:3000", "https://localhost:3000", "http://localhost:3001", "https://effortless-daffodil-9a61f8.netlify.app", "https://timetableapp-cristi.netlify.app")
            .AllowAnyMethod()
            .AllowAnyHeader();
        });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI();
//}

app.UseCors("CORSPolicy");
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

using (var cristiContext = new DatabaseContext())
{

    #region insertStudents
    //var student1 = new Student()
    //{
    //    Id = Guid.NewGuid().ToString(),
    //    Name = "Cristi Petrusan",
    //    Age = 21

    //};
    //var student2 = new Student()
    //{
    //    Id = Guid.NewGuid().ToString(),
    //    Name = "Iulia Rus",
    //    Age = 22

    //};

    //_cristiContext.Students?.Add(student1);
    //_cristiContext.Students?.Add(student2);

    //await _cristiContext.SaveChangesAsync();
    #endregion
    #region getStudents
    //if (_cristiContext.Students != null)
    //{
    //    var students = await _cristiContext.Students.ToListAsync();
    //    foreach(var student in students)
    //    {
    //        Console.WriteLine(student.Id);
    //        Console.WriteLine(student.Name);
    //        Console.WriteLine(student.Age);
    //    }
    //}
    #endregion
}


app.Run();
