module.exports = {
    resolve: {
        fallback: {
            path: require.resolve("path-browserify/"),
            fs: false,
            assert: require.resolve("assert/"),
            crypto: require.resolve("crypto-browserify/"),
            url: require.resolve("url/"),
            https: require.resolve("https-browserify"),
        }
    }

};