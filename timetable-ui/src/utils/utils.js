import { RRule, rrulestr } from 'rrule';

export const API_URL = process.env.NODE_ENV === 'development'
    ? process.env.REACT_APP_API_LOCAL
    : process.env.REACT_APP_API_DEPLOYED;

export const transformToSentenceCase = (camelCase) => {
    const regex = /(?<=[a-z])[A-Z]/g;
    const sentenceCase = camelCase.replace(regex, (match) => ` ${match.toLowerCase()}`);
    return sentenceCase.charAt(0).toUpperCase() + sentenceCase.slice(1);
};

export const getNextSevenDaysAppointments = (appointments) => {
    let now = new Date();
    let sevenDaysLater = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 7);

    return appointments.filter(appointment => {
        if (appointment.rRule) {
            console.log("if" + appointment.title);

            let rRule = new RRule({
                ...rrulestr(appointment.rRule),
                // dtstart: new Date(appointment.startDate),
                // until: new Date(appointment.endDate)
            });

            console.log(rRule);

            // Use 'between' method to get all occurrences between two dates
            let nextOccurrences = rRule.between(now, sevenDaysLater);
            console.log(rRule.between(now, sevenDaysLater));

            let nextOccurrence = nextOccurrences[0];

            return nextOccurrence ? true : false;
        } else {
            console.log("else" + appointment.title);
            let startDate = new Date(appointment.startDate);
            return startDate.valueOf() > now.valueOf() && startDate.valueOf() < sevenDaysLater.valueOf();
        }
    });
};

