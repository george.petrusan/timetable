import logo from './logo.svg';
import './styles/global.css';
import './styles/App.css';
import { Routes, Router, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { GoogleLogin } from '@react-oauth/google';
import { GoogleOAuthProvider } from '@react-oauth/google';
import jwt_decode from 'jwt-decode';
import { decode } from 'punycode';
import { Grid, Typography, Button, Card, CardContent, Container, Paper } from '@mui/material';
import { Link } from 'react-router-dom';
import EditStudents from './pages/EditStudents';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
// import Tab1 from './Tab1';
import ModifyAppointments from './components/ModifyAppointments';
import StudentOptions from './pages/StudentOptions';
import DynamicPage from './pages/DynamicPage';
import SubjectsEditor from './pages/SubjectsEditor';
// import './styles/styles.css'
import { API_URL } from './utils/utils';
import EditAdministrators from './pages/EditAdministrators';
import EditTeachers from './pages/EditTeachers';
import Repartition from './pages/Repartition'
import Requests from './pages/Requests';
import Footer from './components/Footer';
import { TypeAnimation } from 'react-type-animation';
import { CalendarMonth, Group } from '@mui/icons-material';
import TeachersTimetable from './pages/TeachersTimetable';


function App() {
	const [user, setUser] = useState({});

	const [matriculationCode, setMatriculationCode] = useState('');
	const [role, setRole] = useState('not set');

	async function handleGoogleLoginSuccess(response) {
		const authResponse = response.tokenObj;
		var decoded = jwt_decode(response.credential);
		setUser({email: decoded.email, picture: decoded.picture, name: decoded.name});

		console.log(JSON.stringify(decoded, null, 2));
		console.log(authResponse);
	}

	useEffect(() => {
		if (user && user.email) {
			// Make an API call to retrieve the student data
			fetch(API_URL + `/Administrator/login/${user.email}`)
				.then((response) => {
					if (response.ok) {
						return response.json();
					} else {
						throw new Error('Email not found in any roles.');
					}
				})
				.then((data) => {
					if (data.role === "Student")
						setRole("Student");
					if (data.role === "Teacher")
						setRole("Teacher");
					if (data.role === "Administrator")
						setRole("Administrator");
				})
				.catch((error) => {
					console.error('Error retrieving data:', error.message);
					setRole('not found');
				});
		}
	}, [user]);

	async function login()
	{
		fetch(API_URL + `/Administrator/login/${user.email}`)
			.then((response) => {
				if (response.ok) {
					return response.json();
				} else {
					throw new Error('Email not found in any roles.');
				}
			})
			.then((data) => {
				if (data.role === "Student")
					setRole("Student");
				if (data.role === "Teacher")
					setRole("Teacher");
				if (data.role === "Administrator")
					setRole("Administrator");
			})
			.catch((error) => {
				console.error('Error retrieving data:', error.message);
				setRole('not found');
			});
	}

	const handleGoogleLoginFailure = (error) => {
		console.log(error);
	};

	function handleLogout() {
		setUser({});
		setMatriculationCode('');
		setRole('not set');
	}

	if (!user.email)
	{
		return (
			<Grid container justifyContent="center" alignItems="center" style={{ height: '100vh' }}>
				<Grid item xs={12} sm={8} md={6} lg={4}>
					<Typography variant="h4" align="center" gutterBottom>
						TimetableApp
					</Typography>

					<Typography variant="h5" align="center" gutterBottom>
						<TypeAnimation
							preRenderFirstString={true}
							sequence={[
								1000,
								'Planning tool for students', // initially rendered starting point
								500,
								'Planning tool for teachers',
								500,
								'Planning tool for administrators',
								500,
								'Planning tool for everybody...',
								1000,
							]}
							speed={30}
							repeat={Infinity}
						/>
					</Typography>

					<Grid container spacing={2}>
						<Grid item xs={12} sm={6}>
							<Card sx={{ backgroundColor: '#4285F4', color: '#FFFFFF' }}>
								<CardContent>
									<Typography variant="h6" align="center" color="inherit" gutterBottom>
										Create Personalized Timetables
									</Typography>
									<Typography variant="body2" align="justify" color="inherit">
										Create personalized timetables and meticulously plan your day, week, or month according to your specific requirements.
									</Typography>
								</CardContent>
							</Card>
						</Grid>

						<Grid item xs={12} sm={6}>
							<Card sx={{ backgroundColor: '#EA4335', color: '#FFFFFF' }}>
								<CardContent>
									<Typography variant="h6" align="center" color="inherit" gutterBottom>
										Collaboration Support
									</Typography>
									<Typography variant="body2" align="justify" color="inherit">
										Our app supports collaboration, making it possible for teachers to share timetables with students and vice versa.
									</Typography>
								</CardContent>
							</Card>
						</Grid>

						<Grid item xs={12} sm={6}>
							<Card sx={{ backgroundColor: '#FBBC05', color: '#FFFFFF' }}>
								<CardContent>
									<Typography variant="h6" align="center" color="inherit" gutterBottom>
										Intuitive User Interface
									</Typography>
									<Typography variant="body2" color="inherit" align="justify">
										Our intuitive user interface makes it easy to add, remove, or change events in your timetable.
									</Typography>
								</CardContent>
							</Card>
						</Grid>

						<Grid item xs={12} sm={6}>
							<Card sx={{ backgroundColor: '#34A853', color: '#FFFFFF' }}>
								<CardContent>
									<Typography variant="h6" align="center" color="inherit" gutterBottom>
										Seamless Integration
									</Typography>
									<Typography variant="body2" color="inherit" align="justify">
										Seamlessly integrate your timetable with other platforms such as Google Calendar or Microsoft Outlook.
									</Typography>
								</CardContent>
							</Card>
						</Grid>
					</Grid>

					<Typography variant="body1" align="center" gutterBottom>
						In order to continue, you need to log in.
					</Typography>
					<Grid container justifyContent="center">
						<GoogleOAuthProvider clientId="12318042429-blp7buc1jgn8mqfj04rn4tlrgj9s5da4.apps.googleusercontent.com">
							<GoogleLogin
								onSuccess={handleGoogleLoginSuccess}
								onError={handleGoogleLoginFailure}
								useOneTap
								auto_select
							/>
						</GoogleOAuthProvider>
					</Grid>
				</Grid>
				<Footer></Footer>
			</Grid>
		);
	}

	return (
		<div>
			<AppNavbar 
				user={user} 
				role={role} 
				matriculationCode={matriculationCode} 
				handleLogout={handleLogout}
				handleReload={login}
			></AppNavbar>
			<div className='app-wrapper'>
				<Routes>
					<Route path="/" element={<Home user={user} role={role}/>} />

				{role === "Administrator" && (
					<>
						<Route path="/modifyappointments" element={<ModifyAppointments />} />
						<Route path="/editstudents" element={<EditStudents />} />
						<Route path="/editadministrators" element={<EditAdministrators matriculationCode={matriculationCode} user={user} />} />
						<Route path="/editteachers" element={<EditTeachers matriculationCode={matriculationCode} user={user} />} />
						<Route path="/subjectoptions" element={<SubjectsEditor />} />
						<Route path="/repartition" element={<Repartition />} />
						<Route path="/requests" element={<Requests />} />
					</>)}

				{(role === "Administrator" || role === "Teacher") && (
						<Route path="/teachersTimetable" element={<TeachersTimetable user={user}/>} />
				)}

				{(role === "Administrator" || role === "Student") && (
					<>
						<Route path="/dynamic" element={<DynamicPage matriculationCode={matriculationCode} user={user}/>} />
						<Route path="/options" element={<StudentOptions matriculationCode={matriculationCode} user={user} />} />
					</>)}
				</Routes>
			</div>
			<Footer />
		</div>
	);
}

export default App;
