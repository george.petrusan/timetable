import {
    ViewState,
} from '@devexpress/dx-react-scheduler';
import {
    Scheduler,
    WeekView,
    DayView,
    Appointments,
    AllDayPanel,
    ViewSwitcher,
    TodayButton,
    Toolbar,
    DateNavigator,
    MonthView,
    AppointmentTooltip,
} from '@devexpress/dx-react-scheduler-material-ui';
import React, { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import DownloadIcon from '@mui/icons-material/Download';
import CachedIcon from '@mui/icons-material/Cached';
import { Typography, Box, Divider, TextField } from '@mui/material';
import { Event as EventIcon } from '@mui/icons-material';
import { getNextSevenDaysAppointments } from '../utils/utils';
import { API_URL } from '../utils/utils';

// import { makeStyles } from '@mui/styles';
import { Grid } from '@mui/material';

const AppointmentTooltipContent = ({ appointmentData }) => {
    const { title, startDate, endDate, notes, capacity, teacher } = appointmentData;

    return (
        <Box
            display="flex"
            flexDirection="column"
            alignItems="left"
            sx={{ backgroundColor: '#f5f5f5', padding: '20px' }}
        >
            <Box display="flex" alignItems="center">
                <EventIcon sx={{ marginRight: '0.5rem', color: '#007BFF' }} />
                <Typography variant="h5" sx={{ color: '#333' }}>{title}</Typography>
            </Box>
            <Typography variant="subtitle1" sx={{ marginTop: '10px' }}>
                Start: {new Date(startDate).toLocaleString()}
            </Typography>
            <Typography variant="subtitle1" sx={{ marginTop: '10px' }}>
                End: {new Date(endDate).toLocaleString()}
            </Typography>
            <Typography variant="body1" sx={{ marginTop: '10px' }}>
                Capacity: {capacity}
            </Typography>
            <Typography variant="body1" sx={{ marginTop: '10px' }}>
                Teacher: {teacher}
            </Typography>
            <Divider sx={{ margin: '1rem 0', width: '100%' }} />
            <Typography variant="body1" sx={{ color: '#777' }}>{notes}</Typography>
        </Box>
    );
};

function DynamicPage({matriculationCode, user}) {
    const [appointments, setAppointments] = useState([]);
    const [futureAppointments, setFutureAppointments] = useState([]);

    useEffect(() => {
        fetchAppointments();
    }, []);

    async function fetchAppointments() {
        // const response = await fetch(API_URL + '/appointment/getAppointmentsForStudent/' + matriculationCode);
        const response = await fetch(API_URL + '/appointment/getAppointmentsForStudent/' + user.email);
        const data = await response.json();
        setAppointments(data);
        setFutureAppointments(getNextSevenDaysAppointments(data));
    }

    const handleDownload = async () => {
        // const icsFile = generateICSFile(appointments); // previous way of generating ics file
        // const response = await fetch(API_URL + '/appointment/getICalendarForStudent/' + matriculationCode);
        const response = await fetch(API_URL + '/appointment/getICalendarForStudent/' + user.email);
        const icsFile = await response.text();

        const downloadLink = document.createElement('a');
        downloadLink.setAttribute('href', `data:text/calendar;charset=utf-8,${encodeURIComponent(icsFile)}`);
        downloadLink.setAttribute('download', user.email + '.ics');
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    };

    return (
    <div>
        <span><h4>Student Calendar</h4>
        <Button onClick={fetchAppointments}>
            <CachedIcon></CachedIcon>
            &nbsp;Reload data
        </Button>

        <Button onClick={handleDownload}>
            <DownloadIcon></DownloadIcon>
            Export calendar
                </Button></span>
        <Scheduler data={appointments}> 
            <ViewState defaultCurrentViewName="Week" />
            <DayView startDayHour={8} endDayHour={20}/>
            <WeekView startDayHour={8} endDayHour={20}/>
            <MonthView/>

            <Toolbar />
            <TodayButton />
            <ViewSwitcher />
            <DateNavigator />
            <AllDayPanel />
            {/* <CurrentTimeIndicator
                shadePreviousCells
                shadePreviousAppointments
            /> */}
            
            <Appointments />
            <AppointmentTooltip contentComponent={AppointmentTooltipContent} />
        </Scheduler>

        <h6>Appointments</h6>
        <ul>
            {appointments.map((appointment) => (
                <li key={appointment.id}>{appointment.title}</li>
            ))}
        </ul>
        <h2>Next 7 Days Appointments</h2>
        <ul>
            {futureAppointments.map((appointment) => (
                <li key={appointment.id}>{appointment.title}</li>
            ))}
        </ul>


    </div>
    )
}

export default DynamicPage;
