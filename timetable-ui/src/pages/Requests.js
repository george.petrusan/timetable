import React, { useState, useEffect } from 'react';
import { Button, Dialog, DialogTitle, DialogContent, DialogActions, TextField, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';
import { API_URL } from '../utils/utils';

const Request = ({ email }) => {
    const [requests, setRequests] = useState([]);
    const [selectedRequest, setSelectedRequest] = useState(null);
    const [open, setOpen] = useState(false);
    const [editable, setEditable] = useState(false);
    const [name, setName] = useState('');
    const [matriculationCode, setMatriculationCode] = useState('');

    useEffect(() => {
        fetchRequests();
    }, []);

    const fetchRequests = async () => {
        try {
            const response = await fetch(`${API_URL}/StudentRequest/students`);
            if (response.ok) {
                const data = await response.json();
                setRequests(data);
                console.log(data);
            } else {
                console.error('Error fetching student requests:', response.status);
            }
        } catch (error) {
            console.error('Error fetching student requests:', error);
        }
    };

    const handleDetailsClick = (request) => {
        setSelectedRequest(request);
        setOpen(true);
    };

    const handleDialogClose = () => {
        setOpen(false);
    };

    const handleEditClick = () => {
        setEditable(!editable);
    };

    const handleApproveClick = async () => {
        try {
            // Perform the logic for approving the request

            // Post the request to students
            const approvedRequest = {
                name: selectedRequest.name,
                matriculationCode: selectedRequest.matriculationCode,
                email: selectedRequest.email,
            };

            const response = await fetch(`${API_URL}/students/studentRequest`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(approvedRequest),
            });

            console.log(JSON.stringify(approvedRequest, null, 2));

            if (response.ok) {
                console.log('Request approved and posted to students!');
                handleDisapproveClick();
                // Perform any additional actions after successful request approval and posting to students
            } else {
                console.error('Error posting request to students:', response.status);
                // Handle the error case accordingly
            }
        } catch (error) {
            console.error('Error posting request to students:', error);
            // Handle the error case accordingly
        }

        setEditable(false);
    };

    const handleDisapproveClick = async () => {
        try {
            // Perform the logic for disapproving the request

            // Delete the student request
            const response = await fetch(`${API_URL}/StudentRequest/students/${selectedRequest.email}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                console.log('Request deleted successfully!');
                setOpen(false);
                fetchRequests();
                // Perform any additional actions after successful request disapproval and deletion
            } else {
                console.error('Error deleting request:', response.status);
                // Handle the error case accordingly
            }
        } catch (error) {
            console.error('Error deleting request:', error);
            // Handle the error case accordingly
        }

        setEditable(false);
    };

    return (
        <div>
            <h4>Active Requests</h4>
            <h5>Students:</h5>
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Matriculation Code</TableCell>
                            <TableCell>Email</TableCell>
                            <TableCell>Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {requests.map((request) => (
                            <TableRow key={request.id}>
                                <TableCell>{request.name}</TableCell>
                                <TableCell>{request.matriculationCode}</TableCell>
                                <TableCell>{request.email}</TableCell>
                                <TableCell>
                                    <Button variant="contained" onClick={() => handleDetailsClick(request)}>
                                        Details
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            {requests.length === 0 && (
                <p>No active student requests.</p>
            )}

            {selectedRequest && (
                <Dialog open={open} onClose={handleDialogClose}>
                    <DialogTitle>Request Details</DialogTitle>
                    <DialogContent>
                        <TextField
                            label="Name"
                            value={selectedRequest.name}
                            onChange={(e) => setSelectedRequest({ ...selectedRequest, name: e.target.value })}
                            disabled={!editable}
                            fullWidth
                            margin="normal"
                        />
                        <TextField
                            label="Matriculation Code"
                            value={selectedRequest.matriculationCode}
                            onChange={(e) => setSelectedRequest({ ...selectedRequest, matriculationCode: e.target.value })}
                            disabled={!editable}
                            fullWidth
                            margin="normal"
                        />
                        <TextField
                            label="Email"
                            value={selectedRequest.email}
                            disabled
                            fullWidth
                            margin="normal"
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleDisapproveClick} color="error">
                            Reject
                        </Button>
                        <Button onClick={handleEditClick} color="primary">
                            Edit
                        </Button>
                        <Button onClick={handleApproveClick} color="primary">
                            Approve
                        </Button>
                    </DialogActions>
                </Dialog>
            )}
        </div>
    );
};

export default Request;
