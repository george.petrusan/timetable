import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
// import Checkbox from '@mui/material/Checkbox';
import TableCell from '@mui/material/TableCell';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Autocomplete from '@mui/material/Autocomplete';
import { transformToSentenceCase } from '../utils/utils';

import {
    SelectionState,
    PagingState,
    IntegratedPaging,
    IntegratedSelection,
    EditingState,
    RowDetailState
} from '@devexpress/dx-react-grid';
import { Grid, Table, TableHeaderRow, PagingPanel, TableSelection, TableEditRow, TableEditColumn, TableRowDetail } from '@devexpress/dx-react-grid-material-ui';
import Button from '@mui/material/Button';
import CachedIcon from '@mui/icons-material/Cached';
import TruncatedTableCell from '../components/TruncatedTableCell';
import MultiSelectTextField from '../components/MultiSelectTextField';

const { v4: uuidv4 } = require('uuid');
const API_URL = process.env.NODE_ENV === 'development'
    ? process.env.REACT_APP_API_LOCAL
    : process.env.REACT_APP_API_DEPLOYED;

function EditTeachers() {
    const [currentPage, setCurrentPage] = useState(0);
    const [pageSize, setPageSize] = useState(0);
    const [pageSizes] = useState([10, 25, 50, 0]);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [editedTeacher, setEditedTeacher] = useState(null);
    const [appointmentTitles, setAppointmentTitles] = useState([]);

    const [selection, setSelection] = useState([]);

    const columns = [
        { name: 'name', title: 'Name' },
        { name: 'email', title: 'Email' },
        { name: 'details', title: 'Details', width: 25 },

    ];
    const [rows, setRows] = useState([]);
    const [error, setError] = useState(null);

    const DetailsButtonCell = ({ column, row }) => {
        if (column.name !== 'details') {
            return;
        }

        const handleButtonClick = () => {
            console.log("Clicked: ", row.id);
            setEditedTeacher({ ...row });  // Clone row to editedTeacher
            setDialogOpen(true);  // Open the dialog
            console.log("handleButtonClick");
        };

        const renderTableCells = row => {
            return columns.map(column => {
                if (column.name === 'details') {
                    return (
                        <TableCell key={column.name}>
                            <Button onClick={handleButtonClick}>Details</Button>
                        </TableCell>
                    );
                }

                return (
                    <TruncatedTableCell text={row[column.name]} />
                );
            });
        };


        return (
            <>
                {renderTableCells(row)}
            </>
        );
    };

    const [editingStateColumnExtensions] = useState([
        { columnName: 'id', editingEnabled: false },
    ]);
    const handleTextFieldChange = (event, key, newValue) => {
        if (Array.isArray(newValue)) {
            setEditedTeacher(prevState => ({
                ...prevState,
                [key]: newValue,
            }));
        } else {
            setEditedTeacher(prevState => ({
                ...prevState,
                [key]: event.target.value,
            }));
        }
    };

    const commitChanges = ({ added, changed, deleted }) => {
        const startingAddedId = uuidv4();
        let changedRows;
        console.log("COMMIT CHANGES ")
        console.log("added");
        console.log(added);
        console.log("changed");
        console.log(JSON.stringify(changed));
        console.log("deleted");
        console.log(deleted);
        console.log(JSON.stringify(rows));

        if (added) {
            const newRows = added.map((row, index) => ({
                ...row,
                // id: uuidv4(),
            }));


            changedRows = [
                ...rows,
                ...newRows, // api adding
            ];
            fetch(API_URL + '/teacher', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newRows)
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    fetchData();  // Fetch data again after successful add
                })
                .catch(error => console.error(error));
        }

        setRows(changedRows);
        // fetchData();
    };

    async function handleSave() {
        console.log('Saving...', JSON.stringify(editedTeacher));
        try {
            const updatedAppointmentIds = editedTeacher.appointmentIds;
                // ? editedTeacher.appointmentIds.split(',').map((item) => item.trim())
                // : [];

            const updatedTeacher = {
                ...editedTeacher,
                // appointmentIds: updatedAppointmentIds,
            };
            console.log('Saving...', JSON.stringify(updatedTeacher));

            const response = await fetch(
                `${API_URL}/teacher/updatebasedonid/${editedTeacher.id}`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(updatedTeacher),
                }
            );

            setEditedTeacher(updatedTeacher);

            const responseText = await response.text();
            console.log('Response text:', responseText);

            let data;
            try {
                data = JSON.parse(responseText);
            } catch (error) {
                console.error('Failed to parse response as JSON:', error);
                return;
            }

            console.log('Save response:', data);
            if (!response.ok) {
                console.error('Response error:', response.status);
            } else {
                setDialogOpen(false);
                fetchData();
            }
        } catch (error) {
            console.error('Failed to update teacher:', error);
        }
    }

    const handleDelete = async () => {
        if (window.confirm('Are you sure you want to delete?')) {
            console.log('Deleting...', JSON.stringify(editedTeacher));
            try {
                const response = await fetch(`${API_URL}/teacher/${editedTeacher.id}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(editedTeacher),
                });

                const responseText = await response.text();
                console.log('Response text:', responseText);

                let data;
                try {
                    data = JSON.parse(responseText);
                } catch (error) {
                    console.error('Failed to parse response as JSON:', error);
                    return;
                }

                console.log('Delete response:', data);
                if (!response.ok) {
                    console.error('Response error:', response.status);
                } else {
                    setDialogOpen(false);
                    fetchData();
                }
            } catch (error) {
                console.error('Failed to delete teacher:', error);
            }
        }
    };

    function optionalAppointmentsToString(optionalAppointments) {
        if (!optionalAppointments) {
            return '';
        }

        return Object.entries(optionalAppointments)
            .map(([key, values]) => `${key}:${values.join(',')}`)
            .join(';');
    }

    async function fetchData() {
        try {
            const response = await fetch(API_URL + '/teacher');
            const teachers = await response.json();
            console.log (teachers);

            const transformedTeachers = teachers.map((teacher) => ({
                ...teacher,
                // appointmentIds: teacher.appointmentIds ? teacher.appointmentIds.join(',') : '',
            }));

            setRows(transformedTeachers);
            console.log(transformedTeachers);
        } catch (err) {
            setError(err);
        }

        try {
            const response = await fetch(API_URL + '/appointment/titleList');
            const appointmentTitles = await response.json();

            setAppointmentTitles(appointmentTitles);

            console.log(appointmentTitles);
        } catch (err) {
            setError(err);
            console.log("error: " + err);
        }        
    }

    useEffect(() => {
        fetchData();
    }, []);

    if (error) {
        return <div>There was an error: {error.message}</div>;
    }

    if (!rows) {
        return <div>Loading...</div>;
    }

    return (
        <div>
            <h4>Edit teachers</h4>
            <Button onClick={fetchData}>
                <CachedIcon></CachedIcon>
                &nbsp;Reload data
            </Button>
            <Grid
                rows={rows}
                columns={columns}
            >
                <EditingState
                    onCommitChanges={commitChanges}
                    columnExtensions={editingStateColumnExtensions}
                />

                <PagingState
                    currentPage={currentPage}
                    onCurrentPageChange={setCurrentPage}
                    pageSize={pageSize}
                    onPageSizeChange={setPageSize}
                />

                <SelectionState
                    selection={selection}
                    onSelectionChange={setSelection}
                />

                <IntegratedPaging />
                <IntegratedSelection />
                <Table cellComponent={DetailsButtonCell} />

                <TableHeaderRow />
                <TableEditRow />
                <TableEditColumn
                    showAddCommand
                />
                <PagingPanel
                    pageSizes={pageSizes}
                />
            </Grid>
            <Dialog
                open={dialogOpen}
                onClose={() => setDialogOpen(false)}
                maxWidth='md'
                fullWidth
            >
                <DialogTitle>Teacher Details</DialogTitle>
                <DialogContent>
                    {editedTeacher && Object.entries(editedTeacher).map(([key, value], index) => {
                        switch (key) {
                            case 'id':
                                break;
                            case 'appointmentIds':
                                console.log(value);
                                return (
                                    <Box marginTop={2} marginBottom={2} key={index}>
                                        <Autocomplete
                                            multiple
                                            options={appointmentTitles} // Provide the appointment titles as options
                                            value={value}
                                            onChange={(event, newValue) =>
                                                handleTextFieldChange(event, key, newValue)
                                            }
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    label={transformToSentenceCase(key)}
                                                    fullWidth
                                                />
                                            )}
                                        />
                                    </Box>
                                );

                            default:
                                return (
                                    <Box marginTop={2} marginBottom={2} key={index}>
                                        <TextField
                                            label={transformToSentenceCase(key)}
                                            value={value}
                                            onChange={(event) => handleTextFieldChange(event, key)}
                                            fullWidth
                                        />
                                    </Box>
                                )
                        }
                    })}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDelete} variant="contained" color="error">
                        Delete Teacher
                    </Button>
                    <Button onClick={() => setDialogOpen(false)} color="error">
                        Close
                    </Button>
                    <Button onClick={handleSave} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default EditTeachers;
