import React from 'react';
import "../styles/global.css";
import { Link } from 'react-router-dom';
import { Button, Grid, Card, CardContent, Typography, Stack, CardActionArea } from '@mui/material';
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import AddRequest from '../components/AddRequest';

const API_URL = process.env.NODE_ENV === 'development'
  ? process.env.REACT_APP_API_LOCAL
  : process.env.REACT_APP_API_DEPLOYED;

function Home({ role, matriculationCode, user}) {
	const handleDownload = async () => {
		// const icsFile = generateICSFile(appointments); // previous way of generating ics file
		// const response = await fetch(API_URL + '/appointment/getICalendarForStudent/' + matriculationCode);
		const response = await fetch(API_URL + '/appointment/getICalendarForStudent/' + user.email);
		const icsFile = await response.text();

		const downloadLink = document.createElement('a');
		downloadLink.setAttribute('href', `data:text/calendar;charset=utf-8,${encodeURIComponent(icsFile)}`);
		downloadLink.setAttribute('download', user.email + '.ics');
		document.body.appendChild(downloadLink);
		downloadLink.click();
		document.body.removeChild(downloadLink);
	};

	return (
		<div className="home-container">
			<h1 className="home-title">Welcome!</h1>

			{role && role !== "not set" && role !== "not found" && <h4>As a {role}, you have special access to these features:</h4>}
			{role && role === "not set" && 
				<Box sx={{ display: 'flex' }}>
					<CircularProgress />
				</Box>
			}
			
			{role === "Student" && (
				<Grid container spacing={2}>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#1976D2', color: '#FFFFFF' }}>
							<CardActionArea component={Link} to="/options">
								<CardContent>
									<Typography variant="h6" align="center" color="inherit" gutterBottom>
										Student Options
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#2196F3', color: '#FFFFFF' }}>
							<CardActionArea component={Link} to={`/dynamic`}>
								<CardContent>
									<Typography variant="h6" align="center" color="inherit" gutterBottom>
										View Your Timetable
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#3F51B5', color: '#FFFFFF' }}>
							<CardActionArea onClick={handleDownload}>
								<CardContent>
									<Typography variant="h6" align="center" color="inherit" gutterBottom>
										Export Calendar
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
				</Grid>
			)}
			{role === "Teacher" && (
				<Grid container spacing={2}>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF' }}>
							<CardActionArea component={Link} to={`/teachersTimetable`}>
								<CardContent>
									<Typography variant="h6" align="center" color="inherit" gutterBottom>
										View Your Timetable
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#8BC34A', color: '#FFFFFF' }}>
							<CardActionArea onClick={handleDownload}>
								<CardContent>
									<Typography variant="h6" align="center" color="inherit" gutterBottom>
										Export Calendar
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
					<Grid item xs={12}>
						<Typography variant="h6" align="center" color="inherit" gutterBottom>
							...many more to come :)
						</Typography>
					</Grid>
				</Grid>
			)}
			{role === "Administrator" && (
				<Grid container spacing={2}>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#222f9b', color: '#FFFFFF' }}>
							<CardActionArea component={Link} to="/modifyappointments">
								<CardContent>
									<Typography variant="h6" align="center" gutterBottom sx={{ color: 'inherit' }}>
										Modify Appointments
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#5ac8fa', color: '#FFFFFF' }}>
							<CardActionArea component={Link} to="/subjectoptions">
								<CardContent>
									<Typography variant="h6" align="center" gutterBottom sx={{ color: 'inherit' }}>
										Edit Subjects
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#4cd964', color: '#FFFFFF' }}>
							<CardActionArea component={Link} to="/editstudents">
								<CardContent>
									<Typography variant="h6" align="center" gutterBottom sx={{ color: 'inherit' }}>
										Edit Students
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#ff9500', color: '#FFFFFF' }}>
							<CardActionArea component={Link} to="/editteachers">
								<CardContent>
									<Typography variant="h6" align="center" gutterBottom sx={{ color: 'inherit' }}>
										Edit Teachers
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#5856d6', color: '#FFFFFF' }}>
							<CardActionArea component={Link} to="/editadministrators">
								<CardContent>
									<Typography variant="h6" align="center" gutterBottom sx={{ color: 'inherit' }}>
										Edit Administrators
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6}>
						<Card sx={{ backgroundColor: '#ff2d55', color: '#FFFFFF' }}>
							<CardActionArea component={Link} to="/requests">
								<CardContent>
									<Typography variant="h6" align="center" gutterBottom sx={{ color: 'inherit' }}>
										See Student Requests
									</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
				</Grid>

			)}
			{role === "not found" && (
				<>
					<h4>You do not have a role assigned yet. For adding a student request complete the form:</h4>
					<AddRequest email={user.email}></AddRequest>

					<h6>For a Teacher or Administrator role, contact the other admins.</h6>
				</>
			)}
		</div>
	);
}

export default Home;
