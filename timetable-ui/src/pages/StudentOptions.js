import React, { useState } from 'react';
import DragDropComponent from '../components/DragDropComponent';


function StudentOptions({ matriculationCode, user }) {

    const [selectedValues, setSelectedValues] = useState({});

    function handleSelectedValuesChange(optionName, selectedValues2) {
        setSelectedValues(prevSelectedValues => ({
            ...prevSelectedValues,
            [optionName]: selectedValues2.map(sV => (sV.value))
        }));
    }

    return (
        <div>
            <h4>Student Options</h4>
            <DragDropComponent matriculationCode={matriculationCode} user={user}></DragDropComponent>
        </div>
    );
}

export default StudentOptions;