import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
// import Checkbox from '@mui/material/Checkbox';
import TableCell from '@mui/material/TableCell';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { transformToSentenceCase } from '../utils/utils';

import {
    SelectionState,
    PagingState,
    IntegratedPaging,
    IntegratedSelection,
    EditingState,
    RowDetailState
} from '@devexpress/dx-react-grid';
import { Grid, Table, TableHeaderRow, PagingPanel, TableSelection, TableEditRow, TableEditColumn, TableRowDetail } from '@devexpress/dx-react-grid-material-ui';
import Button from '@mui/material/Button';
import CachedIcon from '@mui/icons-material/Cached';
import TruncatedTableCell from '../components/TruncatedTableCell';
import MultiSelectTextField from '../components/MultiSelectTextField';

const { v4: uuidv4 } = require('uuid');
const API_URL = process.env.NODE_ENV === 'development'
    ? process.env.REACT_APP_API_LOCAL
    : process.env.REACT_APP_API_DEPLOYED;

function SubjectsEditor() {
    const [currentPage, setCurrentPage] = useState(0);
    const [pageSize, setPageSize] = useState(0);
    const [pageSizes] = useState([10, 25, 50, 0]);
    const [selectedStudent, setSelectedStudent] = useState(null);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [editedSubject, setEditedSubject] = useState(null);
    const [appointmentIds, setAppointmentIds] = useState(editedSubject?.appointmentIds || []);
    const [possibleSubjectOptions, setPossibleSubjectOptions] = useState([]);
    const [selectedSubjectOptions, setSelectedSubjectOptions] = useState([]);

    const [selection, setSelection] = useState([]);

    const columns = [
        // { name: 'id', title: 'ID' },
        { name: 'name', title: 'Name' },
        { name: 'studyYear', title: 'Study Year' },
        { name: 'appointments', title: 'Appointments' },
        { name: 'optionalAppointments', title: 'Optional Appointments' },
        { name: 'details', title: 'Details', width: 25 },
    ];
    const [rows, setRows] = useState([]);
    const [error, setError] = useState(null);

    const DetailsButtonCell = ({ column, row }) => {
        if (column.name !== 'details') {
            return;
        }

        const handleButtonClick = () => {
            console.log("Clicked: ", row.id);
            setEditedSubject({ ...row });  // Clone row to editedSubject
            setDialogOpen(true);  // Open the dialog
            console.log("handleButtonClick");
            // console.log(JSON.stringify(selectedSubjectOptions, null, 2));

        };

        const renderTableCells = row => {
            return columns.map(column => {
                if (column.name === 'details') {
                    return (
                        <TableCell key={column.name}>
                            <Button onClick={handleButtonClick}>Details</Button>
                        </TableCell>
                    );
                }

                return (
                    <TruncatedTableCell text={row[column.name]} />
                );
            });
        };


        return (
            <>
                {renderTableCells(row)}
            </>
        );
    };

    const [editingStateColumnExtensions] = useState([
        { columnName: 'id', editingEnabled: false },
    ]);
    const handleTextFieldChange = (event, key) => {
        setEditedSubject(prevState => ({
            ...prevState,
            [key]: event.target.value
        }));
    };

    const commitChanges = ({ added, changed, deleted }) => {
        const startingAddedId = uuidv4();
        let changedRows;
        console.log("COMMIT CHANGES ")
        console.log("added");
        console.log(added);
        console.log("changed");
        console.log(JSON.stringify(changed));
        console.log("deleted");
        console.log(deleted);
        console.log(JSON.stringify(rows));

        if (added) {
            const newRows = added.map((row, index) => ({
                ...row,
                // id: uuidv4(),
            }));


            changedRows = [
                ...rows,
                ...newRows, // api adding
            ];
            fetch(API_URL + '/subject', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newRows)
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    fetchData();  // Fetch data again after successful add
                })
                .catch(error => console.error(error));
        }

        setRows(changedRows);
    };

    async function handleSave() {
        console.log('Saving...', JSON.stringify(editedSubject));
        try {
            const updatedAppointments = editedSubject.appointments
                ? editedSubject.appointments.split(',').map((item) => item.trim())
                : [];
            const updatedOptionalAppointments = editedSubject.optionalAppointments
                ? editedSubject.optionalAppointments.split(',').map((item) => item.trim())
                : [];

            const updatedSubject = {
                ...editedSubject,
                appointments: updatedAppointments,
                optionalAppointments: updatedOptionalAppointments,
            };
            console.log('Saving...', JSON.stringify(updatedSubject));

            const response = await fetch(
                `${API_URL}/subject/${editedSubject.id}`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(updatedSubject),
                }
            );

            setEditedSubject(updatedSubject);

            const responseText = await response.text();
            console.log('Response text:', responseText);

            let data;
            try {
                data = JSON.parse(responseText);
            } catch (error) {
                console.error('Failed to parse response as JSON:', error);
                return;
            }

            console.log('Save response:', data);
            if (!response.ok) {
                console.error('Response error:', response.status);
            } else {
                setDialogOpen(false);
                fetchData();
            }
        } catch (error) {
            console.error('Failed to update subject:', error);
        }
    }

    const handleDelete = async () => {
        if (window.confirm('Are you sure you want to delete?')) {
            console.log('Deleting...', JSON.stringify(editedSubject));
            try {
                const response = await fetch(`${API_URL}/subject/${editedSubject.id}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(editedSubject),
                });

                const responseText = await response.text();
                console.log('Response text:', responseText);

                let data;
                try {
                    data = JSON.parse(responseText);
                } catch (error) {
                    console.error('Failed to parse response as JSON:', error);
                    return;
                }

                console.log('Delete response:', data);
                if (!response.ok) {
                    console.error('Response error:', response.status);
                } else {
                    setDialogOpen(false);
                    fetchData();
                }
            } catch (error) {
                console.error('Failed to delete subject:', error);
            }
        }
    };

    function optionalAppointmentsToString(optionalAppointments) {
        if (!optionalAppointments) {
            return '';
        }

        return Object.entries(optionalAppointments)
            .map(([key, values]) => `${key}:${values.join(',')}`)
            .join(';');
    }

    async function fetchData() {
        try {
            const response = await fetch(API_URL + '/subject');
            const subjects = await response.json();

            const transformedSubjects = subjects.map((subject) => ({
                ...subject,
                appointments: subject.appointments ? subject.appointments.join(',') : '',
                optionalAppointments: subject.optionalAppointments ? subject.optionalAppointments.join(',') : '',

            }));

            setRows(transformedSubjects);
            console.log(transformedSubjects);
        } catch (err) {
            setError(err);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    if (error) {
        return <div>There was an error: {error.message}</div>;
    }

    if (!rows) {
        return <div>Loading...</div>;
    }

    return (
        <div>
            <h4>Edit subjects</h4>
            <Button onClick={fetchData}>
                <CachedIcon></CachedIcon>
                &nbsp;Reload data
            </Button>
            <Grid
                rows={rows}
                columns={columns}
            >
                <EditingState
                    onCommitChanges={commitChanges}
                    columnExtensions={editingStateColumnExtensions}
                />

                <PagingState
                    currentPage={currentPage}
                    onCurrentPageChange={setCurrentPage}
                    pageSize={pageSize}
                    onPageSizeChange={setPageSize}
                />

                <SelectionState
                    selection={selection}
                    onSelectionChange={setSelection}
                />

                <IntegratedPaging />
                <IntegratedSelection />
                <Table cellComponent={DetailsButtonCell} />

                <TableHeaderRow />
                <TableEditRow />
                <TableEditColumn
                    showAddCommand
                />
                <PagingPanel
                    pageSizes={pageSizes}
                />
            </Grid>
            <Dialog
                open={dialogOpen}
                onClose={() => setDialogOpen(false)}
                maxWidth='md'
                fullWidth
            >
                <DialogTitle>Subject Details</DialogTitle>
                <DialogContent>
                    {editedSubject && Object.entries(editedSubject).map(([key, value], index) => {
                        switch (key) {
                            case 'id':
                                break;
                            default:
                                return (
                                    <Box marginBottom={2} key={index}>
                                        <TextField
                                            label={transformToSentenceCase(key)}
                                            value={value}
                                            onChange={(event) => handleTextFieldChange(event, key)}
                                            fullWidth
                                        />
                                    </Box>
                                )
                        }
                    })}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDelete} variant="contained" color="error">
                        Delete Subject
                    </Button>
                    <Button onClick={() => setDialogOpen(false)} color="error">
                        Close
                    </Button>
                    <Button onClick={handleSave} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default SubjectsEditor;
