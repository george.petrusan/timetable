import React, { useState, useEffect } from 'react';
import { Autocomplete, Button, TextField, Box } from '@mui/material';
import { API_URL } from '../utils/utils';

const Repartition = () => {
    const [subjectNames, setSubjectNames] = useState([]);
    const [selectedOptions, setSelectedOptions] = useState([]);
    const [responseText, setResponseText] = useState('');
    const [isEditing, setIsEditing] = useState(false);
    const [error, setError] = useState('');

    const handleSelectedOptionsChange = (event, values) => {
        setSelectedOptions(values);
    };

    const handleButtonClick = () => {
        if (selectedOptions.length === 0) {
            return;
        }

        setIsEditing(false);

        const subjectName = selectedOptions;
        fetch(API_URL + `/subject/repartition/${subjectName}`)
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error while making the repartition');
                }
            })
            .then(data => {
                setResponseText(JSON.stringify(data, null, 2));
                setError('');
            })
            .catch(error => {
                setResponseText('');
                setError(`Error while making the repartition: ${error.message}`);
            });
    };

    const handleEditButtonClick = () => {
        setIsEditing(!isEditing);
    };

    const handleSaveButtonClick = () => {
        setIsEditing(false);
        // Make the POST request to /Subject/MakeRepartition/{SubjectName}
        if (selectedOptions.length === 0) {
            return;
        }
        const subjectName = selectedOptions;
        fetch(API_URL + `/subject/MakeRepartition/${subjectName}`, {
            method: 'POST',
            body: responseText, // Pass the responseText as the request body
            headers: {
                'Content-Type': 'application/json', // Set the appropriate content type
            },
        })
            .then(response => {
                if (response.ok) {
                    console.log('Repartition saved successfully.');
                } else {
                    console.error('Error saving repartition:', response.statusText);
                }
            })
            .catch(error => {
                console.error('Error saving repartition:', error);
            });
    };

    const handleResponseTextChange = (event) => {
        setResponseText(event.target.value);
    };

    useEffect(() => {
        fetch(API_URL + '/subject/names')
            .then(response => response.json())
            .then(data => {
                setSubjectNames(data);
            })
            .catch(error => {
                console.error('Error fetching subject names:', error);
            });
    }, []);

    return (
        <div>
            <h4>Make repartitions for optional Subjects</h4>
            <Box marginTop={2} marginBottom={2}>
                <Autocomplete
                    options={subjectNames}
                    onChange={handleSelectedOptionsChange}
                    renderInput={params => (
                        <TextField {...params} label="Select Subject" variant="outlined" />
                    )}
                />
            </Box>
            <Box marginTop={2} marginBottom={2}>
                <Button onClick={handleButtonClick}>
                    Get Repartition
                </Button>
                <Button
                    onClick={handleEditButtonClick}
                    disabled={!responseText}
                >
                    Edit
                </Button>
                <Button
                    onClick={handleSaveButtonClick}
                    disabled={!isEditing}
                >
                    Save
                </Button>
            </Box>
            <Box marginTop={2} marginBottom={2}>
                <TextField
                    multiline
                    rows={25}
                    value={responseText}
                    label="Repartition Response"
                    variant="outlined"
                    disabled={!isEditing}
                    fullWidth
                    error={!!error}
                    helperText={error}
                    onChange={handleResponseTextChange}
                />
            </Box>
        </div>
    );
};

export default Repartition;
