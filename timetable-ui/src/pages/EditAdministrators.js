import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
// import Checkbox from '@mui/material/Checkbox';
import TableCell from '@mui/material/TableCell';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { transformToSentenceCase } from '../utils/utils';

import {
    SelectionState,
    PagingState,
    IntegratedPaging,
    IntegratedSelection,
    EditingState,
    RowDetailState
} from '@devexpress/dx-react-grid';
import { Grid, Table, TableHeaderRow, PagingPanel, TableSelection, TableEditRow, TableEditColumn, TableRowDetail } from '@devexpress/dx-react-grid-material-ui';
import Button from '@mui/material/Button';
import CachedIcon from '@mui/icons-material/Cached';
import TruncatedTableCell from '../components/TruncatedTableCell';
import MultiSelectTextField from '../components/MultiSelectTextField';

const { v4: uuidv4 } = require('uuid');
const API_URL = process.env.NODE_ENV === 'development'
    ? process.env.REACT_APP_API_LOCAL
    : process.env.REACT_APP_API_DEPLOYED;

function EditAdministrators() {
    const [currentPage, setCurrentPage] = useState(0);
    const [pageSize, setPageSize] = useState(0);
    const [pageSizes] = useState([10, 25, 50, 0]);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [editedAdministrator, setEditedAdministrator] = useState(null);

    const [selection, setSelection] = useState([]);

    const columns = [
        { name: 'name', title: 'Name' },
        { name: 'email', title: 'Email' },
        { name: 'details', title: 'Details', width: 25 },

    ];
    const [rows, setRows] = useState([]);
    const [error, setError] = useState(null);

    const DetailsButtonCell = ({ column, row }) => {
        if (column.name !== 'details') {
            return;
        }

        const handleButtonClick = () => {
            console.log("Clicked: ", row.id);
            setEditedAdministrator({ ...row });  // Clone row to editedAdministrator
            setDialogOpen(true);  // Open the dialog
            console.log("handleButtonClick");
        };

        const renderTableCells = row => {
            return columns.map(column => {
                if (column.name === 'details') {
                    return (
                        <TableCell key={column.name}>
                            <Button onClick={handleButtonClick}>Details</Button>
                        </TableCell>
                    );
                }

                return (
                    <TruncatedTableCell text={row[column.name]} />
                );
            });
        };


        return (
            <>
                {renderTableCells(row)}
            </>
        );
    };

    const [editingStateColumnExtensions] = useState([
        { columnName: 'id', editingEnabled: false },
    ]);
    const handleTextFieldChange = (event, key) => {
        setEditedAdministrator(prevState => ({
            ...prevState,
            [key]: event.target.value
        }));
    };

    const commitChanges = ({ added, changed, deleted }) => {
        const startingAddedId = uuidv4();
        let changedRows;
        console.log("COMMIT CHANGES ")
        console.log("added");
        console.log(added);
        console.log("changed");
        console.log(JSON.stringify(changed));
        console.log("deleted");
        console.log(deleted);
        console.log(JSON.stringify(rows));

        if (added) {
            const newRows = added.map((row, index) => ({
                ...row,
                // id: uuidv4(),
            }));


            changedRows = [
                ...rows,
                ...newRows, // api adding
            ];
            fetch(API_URL + '/administrator', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newRows)
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    fetchData();  // Fetch data again after successful add
                })
                .catch(error => console.error(error));
        }

        setRows(changedRows);
        // fetchData();
    };

    async function handleSave() {
        console.log('Saving...', JSON.stringify(editedAdministrator));
        try {
            const updatedAdministrator = {
                ...editedAdministrator,
            };
            console.log('Saving...', JSON.stringify(updatedAdministrator));

            const response = await fetch(
                `${API_URL}/administrator/updatebasedonid/${editedAdministrator.id}`,
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(updatedAdministrator),
                }
            );

            setEditedAdministrator(updatedAdministrator);

            const responseText = await response.text();
            console.log('Response text:', responseText);

            let data;
            try {
                data = JSON.parse(responseText);
            } catch (error) {
                console.error('Failed to parse response as JSON:', error);
                return;
            }

            console.log('Save response:', data);
            if (!response.ok) {
                console.error('Response error:', response.status);
            } else {
                setDialogOpen(false);
                fetchData();
            }
        } catch (error) {
            console.error('Failed to update administrator:', error);
        }
    }

    const handleDelete = async () => {
        if (window.confirm('Are you sure you want to delete?')) {
            console.log('Deleting...', JSON.stringify(editedAdministrator));
            try {
                const response = await fetch(`${API_URL}/administrator/${editedAdministrator.id}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(editedAdministrator),
                });

                const responseText = await response.text();
                console.log('Response text:', responseText);

                let data;
                try {
                    data = JSON.parse(responseText);
                } catch (error) {
                    console.error('Failed to parse response as JSON:', error);
                    return;
                }

                console.log('Delete response:', data);
                if (!response.ok) {
                    console.error('Response error:', response.status);
                } else {
                    setDialogOpen(false);
                    fetchData();
                }
            } catch (error) {
                console.error('Failed to delete administrator:', error);
            }
        }
    };

    function optionalAppointmentsToString(optionalAppointments) {
        if (!optionalAppointments) {
            return '';
        }

        return Object.entries(optionalAppointments)
            .map(([key, values]) => `${key}:${values.join(',')}`)
            .join(';');
    }

    async function fetchData() {
        try {
            const response = await fetch(API_URL + '/administrator');
            const administrators = await response.json();

            const transformedAdministrators = administrators.map((administrator) => ({
                ...administrator,
            }));

            setRows(transformedAdministrators);
            console.log(transformedAdministrators);
        } catch (err) {
            setError(err);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    if (error) {
        return <div>There was an error: {error.message}</div>;
    }

    if (!rows) {
        return <div>Loading...</div>;
    }

    return (
        <div>
            <h4>Edit administrators</h4>
            <Button onClick={fetchData}>
                <CachedIcon></CachedIcon>
                &nbsp;Reload data
            </Button>
            <Grid
                rows={rows}
                columns={columns}
            >
                <EditingState
                    onCommitChanges={commitChanges}
                    columnExtensions={editingStateColumnExtensions}
                />

                <PagingState
                    currentPage={currentPage}
                    onCurrentPageChange={setCurrentPage}
                    pageSize={pageSize}
                    onPageSizeChange={setPageSize}
                />

                <SelectionState
                    selection={selection}
                    onSelectionChange={setSelection}
                />

                <IntegratedPaging />
                <IntegratedSelection />
                <Table cellComponent={DetailsButtonCell} />

                <TableHeaderRow />
                <TableEditRow />
                <TableEditColumn
                    showAddCommand
                />
                <PagingPanel
                    pageSizes={pageSizes}
                />
            </Grid>
            <Dialog
                open={dialogOpen}
                onClose={() => setDialogOpen(false)}
                maxWidth='md'
                fullWidth
            >
                <DialogTitle>Administrator Details</DialogTitle>
                <DialogContent>
                    {editedAdministrator && Object.entries(editedAdministrator).map(([key, value], index) => {
                        switch (key) {
                            case 'id':
                                break;
                            default:
                                return (
                                    <Box marginTop={2} marginBottom={2} key={index}>
                                        <TextField
                                            label={transformToSentenceCase(key)}
                                            value={value}
                                            onChange={(event) => handleTextFieldChange(event, key)}
                                            fullWidth
                                        />
                                    </Box>
                                )
                        }
                    })}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDelete} variant="contained" color="error">
                        Delete Administrator
                    </Button>
                    <Button onClick={() => setDialogOpen(false)} color="error">
                        Close
                    </Button>
                    <Button onClick={handleSave} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default EditAdministrators;
