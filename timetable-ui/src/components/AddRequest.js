import React, { useState, useEffect } from 'react';
import { Button, Dialog, DialogTitle, DialogContent, DialogActions, TextField } from '@mui/material';
import { API_URL } from '../utils/utils';

const AddRequest = ({ email }) => {
    const [studentRequest, setStudentRequest] = useState(null);
    const [open, setOpen] = useState(false);
    const [name, setName] = useState('');
    const [matriculationCode, setMatriculationCode] = useState('');

    useEffect(() => {
        fetchStudentRequest();
    }, []);

    const fetchStudentRequest = async () => {
        try {
            const response = await fetch(`${API_URL}/StudentRequest/students/${email}`);
            if (response.ok) {
                const data = await response.json();
                setStudentRequest(data);
                setName(data.name);
                setMatriculationCode(data.matriculationCode);
                console.log(data);
            } else {
                console.error('Error fetching student request:', response.status);
            }
        } catch (error) {
            console.error('Error fetching student request:', error);
        }
    };

    const handleAddClick = () => {
        setOpen(true);
    };

    const handleDialogClose = () => {
        setOpen(false);
    };

    const handleUpdateClick = async () => {
        const updatedRequest = {
            name: name,
            matriculationCode: matriculationCode,
            email: email,
        };

        try {
            const response = await fetch(`${API_URL}/StudentRequest/students/updatebasedonemail/${email}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(updatedRequest),
            });

            if (response.ok) {
                console.log('Request updated successfully!');
                fetchStudentRequest();
                // Perform any additional actions after successful request update
            } else {
                console.error('Error updating request:', response.status);
                // Handle the error case accordingly
            }
        } catch (error) {
            console.error('Error updating request:', error);
            // Handle the error case accordingly
        }

        setOpen(false);
    };

    const handleDeleteClick = async () => {
        try {
            const response = await fetch(`${API_URL}/StudentRequest/students/${email}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                console.log('Request deleted successfully!');
                fetchStudentRequest();
                setStudentRequest(null);
                // Perform any additional actions after successful request deletion
            } else {
                console.error('Error deleting request:', response.status);
                // Handle the error case accordingly
            }
        } catch (error) {
            console.error('Error deleting request:', error);
            // Handle the error case accordingly
        }

        setOpen(false);
    };

    const handleSubmit = async () => {
        const newRequest = {
            name: name,
            matriculationCode: matriculationCode,
            email: email,
        };

        try {
            const response = await fetch(`${API_URL}/StudentRequest/students`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify([newRequest]),
            });

            if (response.ok) {
                console.log('Request added successfully!');
                fetchStudentRequest();
                // Perform any additional actions after successful request creation
            } else {
                console.error('Error adding request:', response.status);
                // Handle the error case accordingly
            }
        } catch (error) {
            console.error('Error adding request:', error.message);
            // Handle the error case accordingly
        }

        setOpen(false);
    };

    return (
        <div>
            {studentRequest ? (
                <>
                    <Button variant="contained" onClick={() => setOpen(true)} sx={{ m: 1 }}>
                        Edit/Delete Request
                    </Button>
                    <Dialog open={open} onClose={handleDialogClose}>
                        <DialogTitle>Edit/Delete Student Request</DialogTitle>
                        <DialogContent>
                            <TextField
                                label="Name"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                fullWidth
                                margin="normal"
                                sx={{ mb: 2 }}
                            />
                            <TextField
                                label="Matriculation Code"
                                value={matriculationCode}
                                onChange={(e) => setMatriculationCode(e.target.value)}
                                fullWidth
                                margin="normal"
                                sx={{ mb: 2 }}
                            />
                            <TextField
                                label="Email"
                                value={email}
                                disabled
                                fullWidth
                                margin="normal"
                                sx={{ mb: 2 }}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleUpdateClick} color="primary">
                                Update
                            </Button>
                            <Button onClick={handleDeleteClick} color="secondary">
                                Delete
                            </Button>
                        </DialogActions>
                    </Dialog>
                </>
            ) : (
                <>
                    <Button variant="contained" onClick={() => setOpen(true)} sx={{ m: 1 }}>
                        Add Request
                    </Button>
                    <Dialog open={open} onClose={handleDialogClose}>
                        <DialogTitle>Add Student Request</DialogTitle>
                        <DialogContent>
                            <TextField
                                label="Name"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                fullWidth
                                margin="normal"
                                sx={{ mb: 2 }}
                            />
                            <TextField
                                label="Matriculation Code"
                                value={matriculationCode}
                                onChange={(e) => setMatriculationCode(e.target.value)}
                                fullWidth
                                margin="normal"
                                sx={{ mb: 2 }}
                            />
                            <TextField
                                label="Email"
                                value={email}
                                disabled
                                fullWidth
                                margin="normal"
                                sx={{ mb: 2 }}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleSubmit} color="primary">
                                Add
                            </Button>
                        </DialogActions>
                    </Dialog>
                </>
            )}
        </div>
    );

};

export default AddRequest;
