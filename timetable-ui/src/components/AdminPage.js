import React, { useState, useEffect } from 'react';
import TableCell from '@mui/material/TableCell';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import TextField from '@mui/material/TextField';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import { transformToSentenceCase } from '../utils/utils';

import {
    SelectionState,
    PagingState,
    IntegratedPaging,
    IntegratedSelection,
    EditingState,
    RowDetailState
} from '@devexpress/dx-react-grid';
import { Grid, Table, TableHeaderRow, PagingPanel, TableSelection, TableEditRow, TableEditColumn, TableRowDetail} from '@devexpress/dx-react-grid-material-ui';
import Button from '@mui/material/Button';
import CachedIcon from '@mui/icons-material/Cached';
import TruncatedTableCell from './TruncatedTableCell';
import MultiSelectTextField from './MultiSelectTextField';
import { Autocomplete } from '@mui/material';

const { v4: uuidv4 } = require('uuid');
const API_URL = process.env.NODE_ENV === 'development'
    ? process.env.REACT_APP_API_LOCAL
    : process.env.REACT_APP_API_DEPLOYED;

function AdminPage() {
    const [currentPage, setCurrentPage] = useState(0);
    const [pageSize, setPageSize] = useState(0);
    const [pageSizes] = useState([5, 10, 25, 50, 0]);
    const [selectedStudent, setSelectedStudent] = useState(null);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [editedStudent, setEditedStudent] = useState(null);
    const [appointmentIds, setAppointmentIds] = useState(editedStudent?.appointmentIds || []);
    const [possibleSubjectOptions, setPossibleSubjectOptions] = useState([]);
    const [possibleSubjectOptions2, setPossibleSubjectOptions2] = useState([]);
    const [selectedSubjectOptions, setSelectedSubjectOptions] = useState([]);

    const [selection, setSelection] = useState([]);

    const columns = [
        // { name: 'id', title: 'ID' },
        { name: 'matriculationCode', title: 'Matriculation Code' },
        { name: 'name', title: 'Name' },
        { name: 'department', title: 'Department' },
        { name: 'studyYear', title: 'Study year' },
        { name: 'email', title: "Email" },
        // { name: 'appointmentIds', title: 'Appointment IDs' }, // New column for appointment IDs
        // { name: 'subjects', title: 'Subjects' },
        { name: 'details', title: 'Details' }
    ];
    const [rows, setRows] = useState([]);
    const [error, setError] = useState(null);

    const DetailsButtonCell = ({ column, row }) => {
        if (column.name !== 'details') {
            return;
        }

        const handleButtonClick = () => {
            console.log("Clicked: ", row.id);
            setSelectedStudent(row);
            setEditedStudent({ ...row });  // Clone row to editedStudent
            setDialogOpen(true);  // Open the dialog
            console.log("handleButtonClick");
            console.log(JSON.stringify(row.subjects, null, 2));
            setSelectedSubjectOptions(row.subjects);
            // console.log(JSON.stringify(selectedSubjectOptions, null, 2));
            
        };

        const renderTableCells = row => {
            return columns.map(column => {
                if (column.name === 'details') {
                    return (
                        <TableCell key={column.name}>
                            <Button onClick={handleButtonClick}>Details</Button>
                        </TableCell>
                    );
                }

                return (
                    <TruncatedTableCell text={row[column.name]} />
                );
            });
        };


        return (
            <>
                {renderTableCells(row)}
            </>
        );
    };


    const isValidMatriculationCode = code => /^[A-Z]{2}\d{6}$/.test(code);
    const [editingStateColumnExtensions] = useState([
        { columnName: 'id', editingEnabled: false },
    ]);
    const handleTextFieldChange = (event, key) => {
        setEditedStudent(prevState => ({
            ...prevState,
            [key]: event.target.value
        }));
    };


    const commitChanges = ({ added, changed, deleted }) => {
        const startingAddedId = uuidv4();
        let changedRows;
        if (added) {
            const newRows = added.map((row, index) => ({
                ...row,
                id: uuidv4(),
            }));

            const validNewRows = newRows.filter(row => isValidMatriculationCode(row.matriculationCode));
            const invalidNewRows = newRows.filter(row => !isValidMatriculationCode(row.matriculationCode));

            if (invalidNewRows.length > 0) {
                alert('MATRICULATION CODE NOT VALID');
                return;
            }

            changedRows = [
                ...rows,
                ...validNewRows, // api adding
            ];
            fetch(API_URL + '/students', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(validNewRows)
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    fetchData();  // Fetch data again after successful add
                })
                .catch(error => console.error(error));
        }
        if (changed) {
            let rowIndex = Object.keys(changed)[0];
            changedRows = [...rows];

            // Check if matriculationCode is valid
            if (changed[rowIndex].hasOwnProperty('matriculationCode') && !isValidMatriculationCode(changed[rowIndex].matriculationCode)) {
                alert('MATRICULATION CODE NOT VALID or EMPTY');
                return;
            }

            let updatedRow = { ...rows[rowIndex], ...changed[rowIndex] };

            // Convert appointmentIds to an array if it's a string
            if (typeof updatedRow.appointmentIds === 'string') {
                updatedRow.appointmentIds = updatedRow.appointmentIds.split(',').map(item => item.trim());
            }

            changedRows[rowIndex] = updatedRow;
            console.log(changedRows[rowIndex].id);
            console.log(JSON.stringify(changedRows[rowIndex]));

            fetch(API_URL + '/students/updatebasedonid/' + changedRows[rowIndex].id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(changedRows[rowIndex])
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    fetchData();  // Fetch data again after successful change
                })
                .catch(error => console.error(error));
        }
        if (deleted) {
            // const deletedSet = new Set(deleted);
            // changedRows = rows.filter(row => !deletedSet.has(row.id));
            changedRows = [...rows];
            console.log(changedRows[deleted[0]].id); // used for api to delete student
            changedRows.splice(deleted[0], 1);
            fetch(API_URL + '/students/' + changedRows[deleted[0]].id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    fetchData();  // Fetch data again after successful deletion
                })
                .catch(error => console.error(error));
        }
        console.log("COMMIT CHANGES ")
        console.log("added");
        console.log(added);
        console.log("changed");
        console.log(JSON.stringify(changed));
        console.log("deleted");
        console.log(deleted); 
        console.log(JSON.stringify(rows));

        setRows(changedRows);
    };

    function transformStringToObject(string) {
        const transformedObject = {};
        const pairs = string.split(',');

        pairs.forEach((pair) => {
            const [key, value] = pair.split(':');
            const options = value.split(',');

            transformedObject[key] = options;
        });

        return transformedObject;
    }

    const handleDelete = async () => {
        if (window.confirm('Are you sure you want to delete?')) {
            console.log('Deleting...', JSON.stringify(editedStudent));
            try {
                const response = await fetch(`${API_URL}/students/${editedStudent.id}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(editedStudent),
                });

                const responseText = await response.text();
                console.log('Response text:', responseText);

                let data;
                try {
                    data = JSON.parse(responseText);
                } catch (error) {
                    console.error('Failed to parse response as JSON:', error);
                    return;
                }

                console.log('Delete response:', data);
                if (!response.ok) {
                    console.error('Response error:', response.status);
                } else {
                    setDialogOpen(false);
                    fetchData();
                }
            } catch (error) {
                console.error('Failed to delete subject:', error);
            }
        }
    };

    async function handleSave() {
        console.log('Saving...', JSON.stringify(editedStudent));
        try {
            // Convert the appointmentIds from string to an array of strings
            const updatedAppointmentIds = editedStudent.appointmentIds.split(',').map(item => item.trim())
               
            // Convert the optionalAppointments to the desired format
            const updatedOptionalAppointments = stringToOptionalAppointments(editedStudent.optionalAppointments);

            const updatedSubjects = Array.isArray(selectedSubjectOptions) ? selectedSubjectOptions.map((subject) => subject.value) : [];

            const updatedStudent = {
                ...editedStudent,
                appointmentIds: updatedAppointmentIds,
                optionalAppointments: updatedOptionalAppointments,
                subjects: updatedSubjects,
            };

            const response = await fetch(`${API_URL}/students/updatebasedonid/${editedStudent.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(updatedStudent),
            });

            // Set the editedStudent to updatedStudent
            setEditedStudent(updatedStudent);

            // Log the response as text
            const responseText = await response.text();
            console.log('Response text:', responseText);

            // Try to parse the response text as JSON
            let data;
            try {
                data = JSON.parse(responseText);
            } catch (error) {
                console.error('Failed to parse response as JSON:', error);
                return;
            }

            console.log('Save response:', data);
            if (!response.ok) {
                console.error('Response error:', response.status);
            }
            else {
                setDialogOpen(false);
                fetchData();
            }
        } catch (error) {
            console.error('Failed to update student:', error);
        }
    }
    function optionalAppointmentsToString(optionalAppointments) {
        if (!optionalAppointments) {
            return '';
        }

        return Object.entries(optionalAppointments)
            .map(([key, values]) => `${key}:${values.join(',')}`)
            .join(';');
    }
    function stringToOptionalAppointments(string) {
        if (!string) {
            return {};
        }

        return string.split(';').reduce((acc, item) => {
            const [key, value] = item.split(':');
            acc[key] = value.split(',');
            return acc;
        }, {});
    }

    async function fetchPossibleSubjectOptions() {
        try {
            const response = await fetch(API_URL + '/subject/list');
            const data = await response.json();
            setPossibleSubjectOptions(data);
        } catch (error) {
            console.error('Error fetching subject options:', error);
        }
    };

    async function fetchData() {
        try {
            const response = await fetch(API_URL + '/students');
            const students = await response.json();

            const transformedStudents = students.map((student) => ({
                ...student,
                appointmentIds: student.appointmentIds ? student.appointmentIds.join(',') : '',
                optionalAppointments: optionalAppointmentsToString(student.optionalAppointments),
                subjects: student.subjects ? student.subjects.map((subject) => ({
                    key: subject.toLowerCase().replace(/\s/g, ''),
                    value: subject
                })) : ""
            }));

            setRows(transformedStudents);
            console.log(transformedStudents);
        } catch (err) {
            setError(err);
        }
    }

    useEffect(() => {
        fetchData();
        fetchPossibleSubjectOptions();
    }, []);

    useEffect(() => {
        console.log(rows);

    }, [rows]);
    // useEffect(() => {
    //     console.log("editedStudent");

    //     console.log(editedStudent);

    // }, [editedStudent]);

    if (error) {
        return <div>There was an error: {error.message}</div>;
    }

    if (!rows) {
        return <div>Loading...</div>;
    }

    return (
        <div>
            <Button onClick={fetchData}>
                <CachedIcon></CachedIcon>
                &nbsp;Reload data
            </Button>
            <Grid
                rows={rows}
                columns={columns}
            >
                <EditingState
                    onCommitChanges={commitChanges}
                    columnExtensions={editingStateColumnExtensions}
                />

                <PagingState
                    currentPage={currentPage}
                    onCurrentPageChange={setCurrentPage}
                    pageSize={pageSize}
                    onPageSizeChange={setPageSize}
                />

                <SelectionState
                    selection={selection}
                    onSelectionChange={setSelection}
                />

                <IntegratedPaging />
                <IntegratedSelection />
                <Table cellComponent={DetailsButtonCell} />

                <TableHeaderRow />
                <TableEditRow />
                <TableEditColumn
                    showAddCommand
                    // showEditCommand
                    // showDeleteCommand
                    // cellComponent={CommandComponent}

                />

                {/* <RowDetailState
                    // defaultExpandedRowIds={[1]}
                />

                <TableRowDetail
                    contentComponent={CommandComponent}
                    // cellComponent={DetailCell}
                    // toggleCellComponent={ToggleCell}
                /> */}


                {/* <TableSelection showSelectAll 
                    selectByRowClick/> */}
                <PagingPanel
                    pageSizes={pageSizes}
                />
            </Grid>
            <Dialog
                open={dialogOpen}
                onClose={() => setDialogOpen(false)}
                maxWidth='md'
                fullWidth
            >
                <DialogTitle>Student Details</DialogTitle>
                <DialogContent>
                    {editedStudent && Object.entries(editedStudent).map(([key, value], index) => {
                        switch (key) {
                            case 'studyYear':
                                const options = [1, 2, 3, 4, 5, 6];
                                return (
                                    <Box marginTop={2} marginBottom={2} key={index}>
                                        <Select
                                            label="Study Year"
                                            value={value}
                                            onChange={(event) => handleTextFieldChange(event, key)}
                                            fullWidth
                                        >
                                            {options.map((option, i) => (
                                                <MenuItem key={i} value={option}>
                                                    {option}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </Box>
                                );
                            case 'id':
                                break;
                            case 'discriminator':
                                break;
                            case 'subjects':
                                return (
                                    <Box marginTop={2} marginBottom={2} key={index}>
                                        {transformToSentenceCase(key)}
                                        <MultiSelectTextField
                                            options={possibleSubjectOptions}
                                            selectedOptions={selectedSubjectOptions}
                                            setSelectedOptions={setSelectedSubjectOptions}
                                        ></MultiSelectTextField>
                                    </Box>
                                );
                            default:
                                return (
                                    <Box marginTop={2} marginBottom={2} key={index}>
                                        <TextField
                                            label={transformToSentenceCase(key)}
                                            value={value}
                                            onChange={(event) => handleTextFieldChange(event, key)}
                                            fullWidth
                                        />
                                    </Box>
                                )
                        }
                    })}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDelete} variant="contained" color="error">
                        Delete Student
                    </Button>
                    <Button onClick={() => setDialogOpen(false)} color="error">
                        Close
                    </Button>
                    <Button onClick={handleSave} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>


        </div>
    );
}

export default AdminPage;
