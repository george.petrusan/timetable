import React, { useEffect, useRef, useState } from 'react';
import { Dialog, DialogTitle, DialogContent, TextField, Button } from '@mui/material';

import DownloadIcon from '@mui/icons-material/Download';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import DeleteIcon from '@mui/icons-material/Delete';
import { API_URL } from '../utils/utils';
import AppointmentsEditor from './AppointmentsEditor';

function ModifyAppointments() {
	const fileInputRef = useRef(null);
	const [importedFileContent, setImportedFileContent] = useState('');
	const [fileName, setFileName] = useState('');
	const [open, setOpen] = useState(false);
	const [subjectData, setSubjectData] = useState('');

	const handleOpenDialog = () => {
		fetch(API_URL + '/subject/withoutAppointment')
			.then(response => response.json())
			.then(data => {
				const transformedData = data.join(',\n');
				setSubjectData(transformedData);
				setOpen(true);
			})
			.catch(error => {
				console.error('Error fetching subject data:', error);
			});
	};

	const handleCloseDialog = () => {
		setOpen(false);
	};


	const handleDownload = () => {
		fetch(API_URL + '/appointment')
			.then((response) => response.json())
			.then((data) => {
				const jsonData = JSON.stringify(data, null, 2);

				const blob = new Blob([jsonData], { type: 'application/json' });
				const url = URL.createObjectURL(blob);

				const link = document.createElement('a');
				link.href = url;
				link.download = 'appointments.json';
				link.click();

				URL.revokeObjectURL(url);
			})
			.catch((error) => {
				console.error('Error exporting appointments:', error);
			});
	};	

	const handleExportCSV = () => {
		fetch(API_URL + '/appointment/csv')
			.then((response) => response.blob())
			.then((blob) => {
				const url = URL.createObjectURL(blob);
				const link = document.createElement('a');
				link.href = url;
				link.download = 'appointments.csv';
				link.click();

				URL.revokeObjectURL(url);
			})
			.catch((error) => {
				console.error('Error exporting appointments as CSV:', error);
			});
	};
	
	const handleUpload = () => {
		// Access the file from the input element
		const file = fileInputRef.current.files[0];
		if (!file) {
			return;
		}


		// Read the contents of the file
		const reader = new FileReader();
		reader.onload = (event) => {
			const fileContent = event.target.result;
			setImportedFileContent(fileContent);
		};
		reader.readAsText(file);
		setFileName(file.name);
	};

	const handleDelete = () => {
		setFileName('');
		setImportedFileContent('');
		fileInputRef.current.value = null;
	};

	const handleSaveWithOverwrite = () => {
		fetch(API_URL + '/Appointment/import/overwrite', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: importedFileContent,
		})
			.then((response) => response.json())
			.then((data) => {
				// Handle the response or any additional logic
				console.log('Appointments saved with overwrite:', data);
			})
			.catch((error) => {
				// Handle the error
				console.error('Error saving appointments with overwrite:', error);
			});
	};

	const handleSaveWithoutOverwrite = () => {
		// Make the API call to save appointments without overwrite
		fetch(API_URL + '/Appointment/import/no-overwrite', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: importedFileContent,
		})
			.then((response) => response.json())
			.then((data) => {
				// Handle the response or any additional logic
				console.log('Appointments saved without overwrite:', data);
			})
			.catch((error) => {
				// Handle the error
				console.error('Error saving appointments without overwrite:', error);
			});
	};

	const handleSaveWithDeletion = () => {
		// Make the API call to save appointments without overwrite
		fetch(API_URL + '/Appointment/import/with-deletion', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: importedFileContent,
		})
			.then((response) => response.json())
			.then((data) => {
				// Handle the response or any additional logic
				console.log('Appointments saved with deletion:', data);
			})
			.catch((error) => {
				// Handle the error
				console.error('Error saving appointments with deletion:', error);
			});
	};



	useEffect(() => {
		console.log(fileName);
		console.log(importedFileContent);
	}, [importedFileContent]);

	return (
		<div>
			{/* {API_URL + '/appointment'} */}
			<h4>Admin page for appointments</h4>
			{fileName !== '' && 
			<>
				{fileName}
				<br></br>
				{/* {importedFileContent} */}
				<div style={{ display: 'flex', gap: '10px' }}>
					<Button onClick={handleDelete} variant="contained" color="error">
						<DeleteIcon />
						Discard
					</Button>
					<Button variant="contained" color="primary" onClick={handleSaveWithOverwrite}>
						Save with Overwrite
					</Button>
					<Button variant="contained" color="primary" onClick={handleSaveWithoutOverwrite}>
						Save without Overwrite
					</Button>
					<Button variant="contained" color="primary" onClick={handleSaveWithDeletion}>
						Save with deletion of old appointments
					</Button>
				</div>


				<br></br>
			</>
			}
			<Button onClick={handleDownload}>
				<DownloadIcon />
				Export JSON
			</Button>
			<Button onClick={handleExportCSV}>
				<DownloadIcon />
				Export CSV
			</Button>
			<Button
				startIcon={<CloudUploadIcon />}
				onClick={() => fileInputRef.current.click()}
			>
				Import JSON
				<input
					type="file"
					accept=".json"
					ref={fileInputRef}
					style={{ display: 'none' }}
					onChange={handleUpload}
				/>
			</Button>
			<Button variant="contained" onClick={handleOpenDialog}>
				Subjects without Appointments
			</Button>


			<AppointmentsEditor></AppointmentsEditor>

			<Dialog open={open} onClose={handleCloseDialog} maxWidth='xl'>
				<DialogTitle>Subjects without Appointments</DialogTitle>
				<DialogContent>
					<TextField
						multiline
						rows={25}
						value={subjectData}
						variant="outlined"
						disabled
						fullWidth
					/>
				</DialogContent>
			</Dialog>

		</div>
	);
}

export default ModifyAppointments;
