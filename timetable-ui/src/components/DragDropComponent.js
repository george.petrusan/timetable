import React, { useEffect, useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import Button from '@mui/material/Button';
import SaveIcon from '@mui/icons-material/Save';
import CachedIcon from '@mui/icons-material/Cached';
import styles from '../styles/DragDropComponent.module.css';
import { API_URL } from "../utils/utils";

// Fake data
const initialStudentData = {
    // "Example": [
    //     "Subject1",
    //     "Subject2"
    // ]
};
// Reordering function
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
};

const DragDropComponent = ({ matriculationCode, user }) => {
    const [studentData, setStudentData] = useState(initialStudentData);
    // const [appointments, setAppointments] = useState(null);

    useEffect(() => {
        fetchAppointments();
    }, [user.email]);

    const fetchAppointments = () => {
        fetch(`${API_URL}/students/email/${user.email}/optionalAppointments`)
            .then(response => response.json())
            .then(data => setStudentData(data))
            .catch(error => console.error('Error:', error));
    };

    const saveAppointments = () => {
        fetch(`${API_URL}/students/email/${user.email}/optionalAppointments`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(studentData),
        })
            .then(response => response.json())
            .then(data => console.log('Success:', data))
            .catch((error) => console.error('Error:', error));
    };

    const onDragEnd = (result, subjectName) => {
        if (!result.destination) return;

        const appointments = studentData[subjectName];
        const reorderedAppointments = reorder(
            appointments,
            result.source.index,
            result.destination.index
        );

        setStudentData({
            ...studentData,
            [subjectName]: reorderedAppointments
        });
    };

    return (
        <DragDropContext onDragEnd={result => onDragEnd(result, result.source.droppableId)}>
            <div className={styles.buttonArea}>
                <Button onClick={fetchAppointments}>
                    <CachedIcon></CachedIcon>
                    &nbsp;Reload data
                </Button>
                <Button onClick={saveAppointments}>
                    <SaveIcon></SaveIcon>
                    &nbsp;Save
                </Button>
            </div>
            {Object.entries(studentData).length === 0 ? (
                <p>You have no options to order :) Please contact the administrator if you want to.</p>
            ) : (
                Object.entries(studentData).map(([subjectName, appointments]) => (
                    <Droppable droppableId={subjectName} key={subjectName}>
                        {(provided) => (
                            <div
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                                className={styles.droppableArea}
                            >
                                <h3>{subjectName}</h3>
                                {Array.isArray(appointments) && appointments.map((appointment, index) => (
                                    <Draggable key={appointment} draggableId={appointment} index={index}>
                                        {(provided) => (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                className={styles.draggableItem}
                                            >
                                                {appointment}
                                            </div>
                                        )}
                                    </Draggable>
                                ))}
                                {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                ))
            )}
        </DragDropContext>
    );
};

export default DragDropComponent;
