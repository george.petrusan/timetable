import React, { useState } from 'react';
import Multiselect from 'multiselect-react-dropdown';

const MultiSelectTextField = ({ options, selectedOptions, setSelectedOptions }) => {

    const handleOptionChange = (selectedList) => {
        console.log("handleOptionChange");
        setSelectedOptions(selectedList);
        console.log(JSON.stringify(selectedList, null, 2));
        console.log(JSON.stringify(selectedOptions, null, 2));
    };

    return (
        <div>
            <Multiselect
                label="salut"
                options={options}
                selectedValues={selectedOptions}
                onSelect={handleOptionChange}
                onRemove={handleOptionChange}
                displayValue="value"
            />
        </div>
    );
};

export default MultiSelectTextField;
