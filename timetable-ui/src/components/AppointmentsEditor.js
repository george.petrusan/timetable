import React, { useState, useEffect } from 'react';
import { ViewState, EditingState } from '@devexpress/dx-react-scheduler';
import {
	Scheduler,
	WeekView,
	DayView,
	Appointments,
	DragDropProvider,
	EditRecurrenceMenu,
	AllDayPanel,
	ViewSwitcher,
	TodayButton,
	Toolbar,
	CurrentTimeIndicator,
	DateNavigator,
	MonthView,
	AppointmentTooltip,
	ConfirmationDialog,
	AppointmentForm,
} from '@devexpress/dx-react-scheduler-material-ui';

import { Button, TextField, Box } from '@mui/material';
import CachedIcon from '@mui/icons-material/Cached';
import { API_URL } from '../utils/utils';

const recurrenceAppointments = [];
const { v4: uuidv4 } = require('uuid');

const dragDisableIds = new Set([3, 8, 10, 12]);

const allowDrag = ({ id }) => !dragDisableIds.has(id);
const appointmentComponent = (props) => {
	if (allowDrag(props.data)) {
		return <Appointments.Appointment {...props} />;
	}
	return <Appointments.Appointment {...props} style={{ ...props.style, cursor: 'not-allowed' }} />;
};

const CustomTooltipContent = ({ appointmentData, onCommitChanges, ...restProps }) => {
	const DefaultContent = AppointmentTooltip.Content;

	const [formState, setFormState] = useState({
		capacity: appointmentData.capacity || '',
		teacher: appointmentData.teacher || '',
		notes: appointmentData.notes || ''
	});

	// Update form state when appointment data changes
	useEffect(() => {
		setFormState({
			capacity: appointmentData.capacity || '',
			teacher: appointmentData.teacher || '',
			notes: appointmentData.notes || ''
		});
	}, [appointmentData]);

	const handleFieldChange = (field, value) => {
		setFormState(prevState => ({ ...prevState, [field]: value }));
	};

	const handleSubmit = () => {
		onCommitChanges({
			changed: {
				[appointmentData.id]: {
					...appointmentData,
					...formState,
				}
			}
		});
	};

	return (
		<Box
			sx={{
				width: '100%',
				padding: '16px',
				backgroundColor: '#f5f5f5',
				borderRadius: '4px'
			}}
		>
			<DefaultContent {...restProps} appointmentData={appointmentData} />

			<Box sx={{ mt: 2 }}>
				<TextField
					label="Capacity"
					value={formState.capacity}
					onChange={(e) => handleFieldChange('capacity', parseInt(e.target.value))}
					type="number"
					fullWidth
				/>
			</Box>

			<Box sx={{ mt: 2 }}>
				<TextField
					label="Teacher"
					value={formState.teacher}
					onChange={(e) => handleFieldChange('teacher', e.target.value)}
					fullWidth
				/>
			</Box>

			<Box sx={{ mt: 2 }}>
				<TextField
					label="Notes"
					value={formState.notes}
					onChange={(e) => handleFieldChange('notes', e.target.value)}
					fullWidth
				/>
			</Box>

			<Box sx={{ mt: 2 }}>
				<Button onClick={handleSubmit} variant="contained" color="primary" fullWidth>
					Submit
				</Button>
			</Box>

		</Box>
	);
};


const AppointmentsEditor = () => {
	const [data, setData] = useState(recurrenceAppointments);
	const [currentDate, setCurrentDate] = useState(new Date());

	const onCommitChanges = ({ added, changed, deleted }) => {
		const startingAddedId = uuidv4();
		console.log("onCommitChanges");
		console.log("added");
		console.log(added);
		console.log("changed");
		console.log(changed);
		console.log("deleted");

		let updatedData = [...data];

		if (added) {
			updatedData = [...updatedData, { id: startingAddedId, ...added }];
		}

		if (changed) {
			updatedData = updatedData.map((appointment) => (changed[appointment.id] ? { ...appointment, ...changed[appointment.id] } : appointment));
		}

		if (deleted !== undefined) {
			updatedData = updatedData.filter((appointment) => appointment.id !== deleted);
		}

		setData(updatedData);

		if (added) {
			fetch(API_URL + '/appointment/addAppointment', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({ id: startingAddedId, ...added }),
			})
				.then((response) => response.json())
				.then((data) => {
					console.log(data);
					fetchData();
				})
				.catch((error) => console.error(error));
		}

		if (changed) {
			Object.entries(changed).forEach(([id, { title, startDate, endDate, rRule, exDate, allDay, notes, capacity, teacher }]) => {
				const requestBody = { id };

				if (title !== undefined) {
					requestBody.title = title;
				}

				if (startDate !== undefined) {
					requestBody.startDate = startDate;
				}

				if (endDate !== undefined) {
					requestBody.endDate = endDate;
				}

				if (rRule !== undefined) {
					requestBody.rRule = rRule;
				}

				if (exDate !== undefined) {
					requestBody.exDate = exDate;
				}

				if (allDay !== undefined) {
					requestBody.allDay = allDay;
				}

				if (notes !== undefined) {
					requestBody.notes = notes;
				}

				if (capacity !== undefined) {
					requestBody.capacity = capacity;
				}

				if (teacher !== undefined) {
					requestBody.teacher = teacher;
				}


				fetch(API_URL + '/appointment/updatebasedonid/' + id, {
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(requestBody),
				})
					.then((response) => response.json())
					.then((data) => {
						console.log(data);
						fetchData();
					})
					.catch((error) => console.error(error));
			});
		}

		if (deleted) {
			fetch(API_URL + '/appointment/' + deleted, {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json',
				},
			})
				.then((response) => response.json())
				.then((data) => {
					console.log(data);
					fetchData();
				})
				.catch((error) => console.error(error));
		}
	};

	async function fetchData() {
		try {
			const response = await fetch(API_URL + '/Appointment');
			const data = await response.json();
			setData(data);
		} catch (error) {
			console.error(error);
			alert('Connection NOT successful!');
		}
	}

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<>
			<Button onClick={fetchData}>
				<CachedIcon></CachedIcon>
				&nbsp;Reload data
			</Button>

			<Scheduler data={data} height={800}>
				<ViewState defaultCurrentDate={currentDate} defaultCurrentViewName="Week" />
				<EditingState onCommitChanges={onCommitChanges} />
				<EditRecurrenceMenu />
				<DayView startDayHour={8} endDayHour={20} />
				<WeekView startDayHour={8} endDayHour={20} />
				<MonthView />
				<Toolbar />
				<TodayButton />
				<ViewSwitcher />
				<ConfirmationDialog />
				<Appointments appointmentComponent={appointmentComponent} />
				<AppointmentTooltip
					showOpenButton
					showDeleteButton
					contentComponent={props => <CustomTooltipContent {...props} onCommitChanges={onCommitChanges} />}
				/>
				<AppointmentForm />
				<AllDayPanel />
				<DragDropProvider allowDrag={allowDrag} />
				<CurrentTimeIndicator shadePreviousCells shadePreviousAppointments />
				<DateNavigator />
			</Scheduler>
		</>
	);
};

export default AppointmentsEditor;
