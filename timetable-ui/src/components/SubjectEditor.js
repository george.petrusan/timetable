import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { API_URL } from '../utils/utils';

function SubjectForm({ onAddSubjects, onEditSubject, isEditMode, editIndex, editingSubject }) {
    const [subjects, setSubjects] = useState([{ name: '', studyYear: '', courseAppointments: '', labAppointments: '' }]);

    useEffect(() => {
        if (editingSubject) {
            setSubjects([editingSubject]);
        }
    }, [editingSubject]);

    const handleSubjectChange = (index, field, value) => {
        const updatedSubjects = [...subjects];
        updatedSubjects[index][field] = value;
        setSubjects(updatedSubjects);
    };

    const handleAddSubject = () => {
        setSubjects([...subjects, { name: '', studyYear: '', courseAppointments: '', labAppointments: '' }]);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const updatedSubjects = subjects.map((subject) => ({
            name: subject.name,
            studyYear: subject.studyYear,
            courseAppointments: typeof subject.courseAppointments === 'string'
                ? subject.courseAppointments.split(',').map((item) => item.trim())
                : subject.courseAppointments,
            labAppointments: typeof subject.labAppointments === 'string'
                ? subject.labAppointments.split(',').map((item) => item.trim())
                : subject.labAppointments,
        }));

        if (isEditMode) {
            // Handle edit mode
            updatedSubjects[editIndex] = subjects[editIndex];
            onEditSubject(editIndex, updatedSubjects);
        } else {
            // Handle add mode
            onAddSubjects(updatedSubjects);
        }

        setSubjects([{ name: '', studyYear: '', courseAppointments: '', labAppointments: '' }]);
    };

    return (
        <form onSubmit={handleSubmit}>
            {subjects.map((subject, index) => (
                <div key={index}>
                    <TextField
                        label="Name"
                        value={subject.name}
                        onChange={(e) => handleSubjectChange(index, 'name', e.target.value)}
                        required
                    />
                    <TextField
                        label="Study Year"
                        value={subject.studyYear}
                        onChange={(e) => handleSubjectChange(index, 'studyYear', e.target.value)}
                        required
                    />
                    <TextField
                        label="Course Appointments"
                        value={subject.courseAppointments}
                        onChange={(e) => handleSubjectChange(index, 'courseAppointments', e.target.value)}
                        required
                    />
                    <TextField
                        label="Lab Appointments"
                        value={subject.labAppointments}
                        onChange={(e) => handleSubjectChange(index, 'labAppointments', e.target.value)}
                        required
                    />
                    {isEditMode && editIndex === index && (
                        <Button type="button" onClick={() => onEditSubject(null, null)}>
                            Cancel
                        </Button>
                    )}
                </div>
            ))}
            <Button type="button" onClick={handleAddSubject}>
                Add Subject
            </Button>
            <Button type="submit">
                {isEditMode ? 'Update' : 'Submit'}
            </Button>
        </form>
    );
}

function SubjectList() {
    const [subjects, setSubjects] = useState([]);
    const [isEditMode, setIsEditMode] = useState(false);
    const [editIndex, setEditIndex] = useState(null);
    const [editingId, setEditingId] = useState(null); // new state variable

    useEffect(() => {
        fetchSubjects();
    }, []);

    const fetchSubjects = async () => {
        try {
            const response = await fetch(`${API_URL}/subject`);
            const data = await response.json();
            setSubjects(data);
        } catch (error) {
            console.error('Error fetching subjects:', error);
        }
    };

    const addSubjects = async (newSubjects) => {
        try {
            const response = await fetch(`${API_URL}/subject`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(newSubjects),
            });
            if (response.ok) {
                fetchSubjects();
            } else {
                console.error('Failed to add subjects');
            }
        } catch (error) {
            console.error('Error adding subjects:', error);
        }
    };

    const editSubject = async (index, updatedSubjects) => {
        try {
            const response = await fetch(`${API_URL}/subject/${subjects[index].id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(updatedSubjects),
            });
            if (response.ok) {
                setIsEditMode(false);
                setEditIndex(null);
                fetchSubjects();
            } else {
                console.error('Failed to update subject');
            }
        } catch (error) {
            console.error('Error updating subject:', error);
        }
    };

    const deleteSubject = async (id) => {
        try {
            const response = await fetch(`${API_URL}/subject/${id}`, {
                method: 'DELETE',
            });
            if (response.ok) {
                fetchSubjects();
            } else {
                console.error('Failed to delete subject');
            }
        } catch (error) {
            console.error('Error deleting subject:', error);
        }
    };

    const handleEditSubject = (index, updatedSubjects) => {
        if (index === null) {
            setIsEditMode(false);
            setEditIndex(null);
            setEditingId(null);
        } else {
            setIsEditMode(true);
            setEditIndex(index);
            setEditingId(subjects[index].id);
            setSubjects(updatedSubjects);
        }
    };

    return (
        <div>
            <h1>Subject List</h1>
            <SubjectForm
                onAddSubjects={addSubjects}
                onEditSubject={handleEditSubject}
                isEditMode={isEditMode}
                editIndex={editIndex}
                editingSubject={isEditMode ? subjects[editIndex] : null}
            />
            {subjects.map((subject, index) => (
                <div key={subject.id}>
                    <TextField
                        label="Name"
                        value={subject.name}
                        disabled={subject.id !== editingId}
                    />
                    {/* <TextField
                        label="Study Year"
                        value={subject.studyYear}
                        disabled={subject.id !== editingId}
                    />
                    <TextField
                        label="Course Appointments"
                        value={Array.isArray(subject.courseAppointments) ? subject.courseAppointments.join(', ') : subject.courseAppointments}
                        disabled={subject.id !== editingId}
                    />
                    <TextField
                        label="Lab Appointments"
                        value={Array.isArray(subject.labAppointments) ? subject.labAppointments.join(', ') : subject.labAppointments}
                        disabled={subject.id !== editingId}
                    /> */}
                    <Button onClick={() => deleteSubject(subject.id)}>Delete</Button>
                    <Button onClick={() => handleEditSubject(index, subjects)}>Edit</Button>
                </div>
            ))}
        </div>
    );

}

export default SubjectList;
