import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import ProfileCard from './ProfileCard';
import { FaHome, FaUser, FaUserCog, FaSignOutAlt, FaRegCalendarAlt, FaSyncAlt } from 'react-icons/fa';

const AppNavbar = ({ user, role, handleLogout, handleReload }) => {
    return (
        <Navbar bg="light" variant="light" expand="md">
            <Navbar.Brand as={Link} to="/" className="navbar-brand">Timetable App</Navbar.Brand>
            <Navbar.Toggle aria-controls="navbar-nav" />
            <Navbar.Collapse id="navbar-nav" className="navbar-brand">
                <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/"><FaHome /> Home</Nav.Link>
                    {/* <Nav.Link as={Link} to="/tab1">Tab1</Nav.Link> */}

                    {role === "Administrator" && (
                        <NavDropdown title={<><FaUserCog /> Administrator</>} id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to="/modifyappointments" activeClassName="active">Modify appointments</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/subjectoptions" activeClassName="active">Edit subjects</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/repartition" activeClassName="active">Repartitions for optional subjects</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item as={Link} to="/editstudents" activeClassName="active">Edit students</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/editteachers" activeClassName="active">Edit teachers</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to="/editadministrators" activeClassName="active">Edit administrators</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item as={Link} to="/requests" activeClassName="active">Requests</NavDropdown.Item>
                        </NavDropdown>
                    )}

                    {(role === "Student" || role === "Administrator") && (
                        <NavDropdown title={<><FaUser /> Student</>} id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to={`/dynamic`} activeClassName="active"><FaRegCalendarAlt /> Student Timetable</NavDropdown.Item>
                            <NavDropdown.Item as={Link} to={`/options`} activeClassName="active">Student Options</NavDropdown.Item>
                        </NavDropdown>
                    )}

                    {(role === "Teacher" || role === "Administrator") && (
                        <NavDropdown title={<><FaUser /> Teacher</>} id="basic-nav-dropdown">
                            <NavDropdown.Item as={Link} to={`/teachersTimetable`} activeClassName="active"><FaRegCalendarAlt /> Teacher Timetable</NavDropdown.Item>
                        </NavDropdown>
                    )}

                    <NavDropdown title={<><FaUser /> Profile</>} id="basic-nav-dropdown">
                        <NavDropdown.Item activeClassName="active" disabled>
                            <ProfileCard user={user} role={role}></ProfileCard>
                        </NavDropdown.Item>
                        <NavDropdown.Item activeClassName="active" onClick={handleReload}><FaSyncAlt /> Reload</NavDropdown.Item>
                        <NavDropdown.Item activeClassName="active" onClick={handleLogout}><FaSignOutAlt /> Logout</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default AppNavbar;
