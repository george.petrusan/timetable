import React from 'react';
import TableCell from '@mui/material/TableCell';

const TruncatedTableCell = ({ text }) => {
    const cellStyle = {
        maxWidth: '200px', // Adjust the value based on your needs
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
    };

    return (
        <TableCell style={cellStyle} title={text}>
            {text}
        </TableCell>
    );
};

export default TruncatedTableCell;
