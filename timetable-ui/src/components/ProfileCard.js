import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import Button from '@mui/material/Button';

const ProfileCard = ({ user, matriculationCode, role }) => {
    return (
        <div>
            <img src={user.picture} alt="Profile Picture" style={{ borderRadius: '50%' }} />
            <h3>{user.name}</h3>
            <p>Email: {user.email}</p>
            <p>Role: {role}</p>
        </div>
    );
};

export default ProfileCard;
